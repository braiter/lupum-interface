module.exports = {
  "lintOnSave": false,
  chainWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      config.module.rule('vue').uses.delete('cache-loader');
      config.module.rule('js').uses.delete('cache-loader');
      config.module.rule('ts').uses.delete('cache-loader');
      config.module.rule('tsx').uses.delete('cache-loader');
    }
  },

  "devServer": {
    "disableHostCheck": true,
    "host": "0.0.0.0",
    "port": 8080,
    "overlay": {
      "warnings": false,
      "errors": false
    },
    "bonjour": true,
    "stats": "verbose"
  },

  "pluginOptions": {
    "i18n": {
      "locale": "en",
      "fallbackLocale": "en",
      "localeDir": "locales",
      "enableInSFC": true
    }
  },

  "pwa": {
    "iconPaths": {
      "favicon16": "favicon.ico",
      "favicon32": "favicon.ico"
    },
    "name": "Test",
    "appleMobileWebAppCapable": true,
    "themeColor": "#272727",
    "msTileColor": "#272727",
    "manifestOptions": {
      "background_color": "#000000"
    },
    "manifestPath": "manifest.json"
  },

  "transpileDependencies": [
    "vuetify"
  ],

  outputDir: 'build',
  assetsDir: 'assets',
  productionSourceMap: false
}
