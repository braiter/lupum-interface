import axios from 'axios'

class CatalogModel {
    getCatalog() {
        return axios.get(`${process.env.VUE_APP_BACKEND_API}/catalog?showAll=true`)
    }

    saveItem(category: any) {
        return axios.post(`${process.env.VUE_APP_BACKEND_API}/catalog`, category)
    }

    updateItem(category: any) {
        return axios.put(`${process.env.VUE_APP_BACKEND_API}/catalog/${category.id}`, category)
    }

    deleteItem(category: any) {
        return axios.delete(`${process.env.VUE_APP_BACKEND_API}/catalog/${category.id}`, category)
    }

    saveStructure(structure: any) {
        return axios.post(`${process.env.VUE_APP_BACKEND_API}/catalog/saveStructure`, structure)
    }
}

export default new CatalogModel();
