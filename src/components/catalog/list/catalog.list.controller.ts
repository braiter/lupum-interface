import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import { Prop } from 'vue-property-decorator';
import {mapState} from "vuex";

const CatalogModel = require('../catalog.model').default;
const EventBus = require('@/plugins/eventBus').default;


@Component({
    computed: mapState(['loggedUser'])
})
export default class CatalogListController extends Core {
    catalog: Array<any> = [];
    plainCatalog: Array<any> = [];
    catalogLevels: Array<any> = [
        [],
        {},
        {},
    ];
    catalogLoaded: boolean = false;
    loggedUser: any;

    currentCatalogLevel: number = 0;

    editCatalog: boolean = false;
    categoryForEdit: any = null;
    editFormValid: boolean = false;

    @Prop() selectCategory;
    @Prop() selectCategoryId;

    mounted() {
        this.getCatalog();
    }

    async getCatalog() {
        EventBus.$emit('showLoader', true);

        await CatalogModel
            .getCatalog()
            .then((res: any) => {
                let data = res.data;
                
                this.plainCatalog = res.data;
                this.catalog = [];

                this.catalogLoaded = true;

                if (data.length) {
                    this.catalogLevels[0] = {
                        children: [],
                    };
                    
                    data.forEach(item => {
                        if (!item.parent) {
                            this.catalog.push({
                                id: item.id,
                                title_en: item.title_en,
                                title_ua: item.title_ua,
                                title_ru: item.title_ru,
                                title_gr: item.title_gr,
                                status: item.status,
                                children: [],
                            });
    
                            this.prepareCatalog(
                                this.catalog[this.catalog.length-1].id,
                                this.catalog[this.catalog.length-1].children,
                                data
                            );

                            this.catalogLevels[0].children.push(this.catalog[this.catalog.length-1])
                        }
                    });
                }
    
                EventBus.$emit('showLoader', false, 500);
            });
    }

    prepareCatalog(
        id: any,
        children: Array<any>,
        data: Array<any>
    ) {
        data.forEach(item => {
            if (item.parent == id) {
                children.push({
                    id: item.id,
                    title_en: item.title_en,
                    title_ua: item.title_ua,
                    title_ru: item.title_ru,
                    title_gr: item.title_gr,
                    parent: item.parent,
                    status: item.status,
                    children: []
                });

                this.prepareCatalog(
                    item.id,
                    children[children.length - 1].children,
                    data
                )
            }
        })
    }

    openNextLevel(listItem: any) {
        this.currentCatalogLevel++;
        this.catalogLevels[this.currentCatalogLevel] = {
            children: listItem.children,
            parent: listItem
        };
        
        console.log(this.catalogLevels);
    }

    openPrevLevel() {
        this.currentCatalogLevel--;
    }

    clickOnItem(
        listItem: any,
        forceOpen: boolean = false
    ) {
        if (listItem.children && listItem.children.length && !forceOpen) {
            this.openNextLevel(listItem);
        }
        else if(!this.selectCategory) {
            this.$router.push(`/catalog/${listItem.id}/products`);
        }
    }

    checkIsCatalogPage(item: any) {
        return this.$route.name == "catalogViewer" && !item.children.length;
    }

    editCategory(item: any) {
        this.categoryForEdit = JSON.parse(JSON.stringify(item));
        this.editCatalog = true;
    }

    async deleteCategory(item: any) {
        const answer = await this.$confirm( "Вы уверены что хотите удалить категорию?", {
            buttonTrueText : "Подтвердить",
            buttonFalseText: "Отмена"
        } );

        if ( answer ) {
            await CatalogModel
                .deleteItem(item)
                .then(res => {
                    EventBus.$emit('showMessage', {...res.data, type: 'success'});
                    this.currentCatalogLevel = 0;
                    this.getCatalog();

                }, error => {
                    EventBus.$emit('showMessage', {...error.response.data, type: 'error', timeout: 3000});
                })
        }
    }

    async saveCategory() {
        EventBus.$emit('showLoader', true);

        await CatalogModel
            .updateItem(this.categoryForEdit)
            .then((res: any) => {
                EventBus.$emit('showLoader', false);
                this.editCatalog = false;
                this.categoryForEdit = null;

                this.getCatalog();

                setTimeout(() => {
                    EventBus.$emit('showMessage', {...res.data, type: 'success'});
                }, 1000);
            });
    }
    
    getCategoryByid(id) {
        return this.plainCatalog.filter(item => item.id == id)[0];
    }
}

