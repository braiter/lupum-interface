import { __decorate, __metadata } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import { Prop } from 'vue-property-decorator';
import { mapState } from "vuex";
const CatalogModel = require('../catalog.model').default;
const EventBus = require('@/plugins/eventBus').default;
let CatalogListController = class CatalogListController extends Core {
    constructor() {
        super(...arguments);
        this.catalog = [];
        this.catalogLevels = [
            {},
            {},
            {},
        ];
        this.currentCatalogLevel = 0;
        this.editCatalog = false;
        this.categoryForEdit = null;
        this.editFormValid = false;
    }
    mounted() {
        this.getCatalog();
    }
    async getCatalog() {
        EventBus.$emit('showLoader', true);
        await CatalogModel
            .getCatalog()
            .then((res) => {
            let data = res.data;
            this.catalog = [];
            this.catalog.push({
                id: data[0].id,
                title: data[0].title,
                children: []
            });
            this.prepareCatalog(this.catalog[0].id, this.catalog[0].children, data);
            this.catalogLevels[0] = this.catalog[0].children;
            this.catalogLevels[0] = {
                items: this.catalog[0].children,
                parent: {
                    title: this.catalog[0].title,
                    id: 'all'
                }
            };
            EventBus.$emit('showLoader', false, 500);
        });
    }
    prepareCatalog(id, children, data) {
        data.forEach(item => {
            if (item.parent == id) {
                children.push({
                    id: item.id,
                    title: item.title,
                    parent: item.parent,
                    children: []
                });
                this.prepareCatalog(item.id, children[children.length - 1].children, data);
            }
        });
    }
    openNextLevel(listItem) {
        this.currentCatalogLevel++;
        this.catalogLevels[this.currentCatalogLevel] = {
            items: listItem.children,
            parent: listItem
        };
    }
    openPrevLevel() {
        this.currentCatalogLevel--;
    }
    clickOnItem(listItem, forceOpen = false) {
        if (listItem.children && listItem.children.length && !forceOpen) {
            this.openNextLevel(listItem);
        }
        else if (!this.selectCategory) {
            this.$router.push(`/catalog/${listItem.id}/products`);
        }
    }
    checkIsCatalogPage(item) {
        return this.$route.name == "catalogViewer" && !item.children.length;
    }
    editCategory(item) {
        this.categoryForEdit = JSON.parse(JSON.stringify(item));
        this.editCatalog = true;
    }
    async deleteCategory(item) {
        const answer = await this.$confirm("Вы уверены что хотите удалить категорию?", {
            buttonTrueText: "Подтвердить",
            buttonFalseText: "Отмена"
        });
        if (answer) {
            await CatalogModel
                .deleteItem(item)
                .then(res => {
                EventBus.$emit('showMessage', { ...res.data, type: 'success' });
                this.currentCatalogLevel = 0;
                this.getCatalog();
            }, error => {
                EventBus.$emit('showMessage', { ...error.response.data, type: 'error', timeout: 3000 });
            });
        }
    }
    async saveCategory() {
        EventBus.$emit('showLoader', true);
        await CatalogModel
            .updateItem(this.categoryForEdit)
            .then((res) => {
            EventBus.$emit('showLoader', false);
            this.editCatalog = false;
            this.categoryForEdit = null;
            this.getCatalog();
            setTimeout(() => {
                EventBus.$emit('showMessage', { ...res.data, type: 'success' });
            }, 1000);
        });
    }
};
__decorate([
    Prop(),
    __metadata("design:type", Object)
], CatalogListController.prototype, "selectCategory", void 0);
__decorate([
    Prop(),
    __metadata("design:type", Object)
], CatalogListController.prototype, "selectCategoryId", void 0);
CatalogListController = __decorate([
    Component({
        computed: mapState(['loggedUser'])
    })
], CatalogListController);
export default CatalogListController;
//# sourceMappingURL=catalog.list.controller.js.map