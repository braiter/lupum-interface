import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';

import { VueNestable, VueNestableHandle } from 'vue-nestable'

const CatalogModel = require('../catalog.model').default;
const ProductModel = require('@/components/product/product.model').default;
const EventBus = require('@/plugins/eventBus').default;


@Component({
    components: {
        VueNestable,
        VueNestableHandle
    }
})
export default class CatalogEditorController extends Core {
    catalog: Array<any> = [];
    formValid: boolean = true;

    categoryPopup: boolean = false;

    categoryForEdit = {
        title_en: '',
        title_ua: '',
        title_ru: '',
        title_gr: '',
        children: [],
        icon: ''
    };
    logos: Array<any> = [];

    structureForSave : Array<any> = [];
    imgPath: string = `${process.env.VUE_APP_BACKEND_API}/product`;

    rules: any = {
        required: (value:any) => {
            return ( typeof value != 'undefined' && value !== null && value.toString().length > 0 ) || "Обязательное поле"
        }
    };

    mounted() {
        this.getCatalog();
    }

    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res: any) => {
                let data = res.data;

                this.catalog = [];
    
                if (data.length) {
                    data.forEach(item => {
                        if (!item.parent) {
                            this.catalog.push({
                                id: item.id,
                                title_en: item.title_en,
                                title_ua: item.title_ua,
                                title_ru: item.title_ru,
                                title_gr: item.title_gr,
                                category_wholesale_price_percantage: item.category_wholesale_price_percantage,
                                category_retail_price_percantage: item.category_retail_price_percantage,
                                children: [],
                                status: item.status,
                                brand_name: item.brand_name,
                                icon: item.icon
                            });
    
                            this.prepareCatalog(
                                this.catalog[this.catalog.length - 1].id,
                                this.catalog[this.catalog.length - 1].children,
                                data
                            );
                        }
                    });
                }
            })
    }

    prepareCatalog(
        id: any,
        children: Array<any>,
        data: Array<any>
    ) {
        data.forEach(item => {
            if (item.parent == id) {
                children.push({
                    id: item.id,
                    title_en: item.title_en,
                    title_ua: item.title_ua,
                    title_ru: item.title_ru,
                    title_gr: item.title_gr,
                    category_wholesale_price_percantage: item.category_wholesale_price_percantage,
                    category_retail_price_percantage: item.category_retail_price_percantage,
                    children: [],
                    status: item.status,
                    brand_name: item.brand_name,
                    icon: item.icon
                });

                this.prepareCatalog(
                    item.id,
                    children[children.length - 1].children,
                    data
                )
            }
        })
    }

    async saveEditForm() {
        await CatalogModel
            .saveItem(this.categoryForEdit)
            .then((res: any) => {
                EventBus.$emit('showMessage', {...res.data, type: 'success'});

                this.categoryPopup = false;
                this.getCatalog();
            })
    }

    categoryPopupOpened(value: any) {
        if (!value) {
            this.categoryForEdit = {
                title_en: '',
                title_ua: '',
                title_ru: '',
                title_gr: '',
                children: [],
                icon: ''
            };
        }
    }

    async openEditCategoryPopup(category: any = {}) {
        if (!category.id) {
            this.categoryForEdit = {
                title_en: '',
                title_ua: '',
                title_ru: '',
                title_gr: '',
                children: [],
                icon: ''
            }
        }
        else {
            this.categoryForEdit = Object.assign({}, category);
        }

        this.categoryPopup = true;
        this.logos = [];
        await this.getLogos(category);
    }

    async saveStructure() {
        this.structureForSave = [];

        this.prepareStructureForSave(
            0,
            this.catalog
        );

        await CatalogModel
            .saveStructure(this.structureForSave)
            .then((res: any) => {
                EventBus.$emit('showMessage', {...res.data, type: 'success'});

                this.structureForSave = [];
            })
    }

    prepareStructureForSave(parentId: any, childrens: Array<any>) {
        childrens.forEach(item => {
            this.structureForSave.push({
                id: item.id,
                title: item.title,
                parent: parentId,
                position: item.position || 1
            });

            if (item.children && item.children.length) {
                this.prepareStructureForSave(item.id, item.children);
            }
        })
    }
    
    async deleteItem(category: any) {
        let confirm = await this.$confirm( "Вы уверены что хотите удалить эту категорию?", {
            buttonTrueText : "Подтвердить",
            buttonFalseText: "Отмена"
        })
        
        if (confirm) {
            await CatalogModel
                .deleteItem(category)
                .then(
                    (res) => {
                        EventBus.$emit('showMessage', {...res.data, type: 'success'});
            
                        this.getCatalog();
                    },
                    (error) => {
                        EventBus.$emit('showMessage', {...error.response.data, type: 'error', timeout: 3000});
                    });
        }
    }
    
    structureUpdated(array) {
        array.forEach((category: any, index: number) => {
            category.position = index + 1;
            
            if (category.children && category.children.length) {
                this.structureUpdated(category.children);
            }
        })
    }

    async getLogos(category) {
        if (!!category.children.length) {
            category.children.forEach(async (item) => {
                await this.getLogos(item);
            });
        } else {
            let res = await ProductModel
                .params({
                    in_stock: false,
                    category: category.id,
                    showAll: true
                })
                .first();

            res.rows.forEach((product) => {
                let images = product.images.map(image => ({...image, product_id: product.id}))
                this.logos = this.logos.concat(images);
            });
        }

    }

    selectAvatar(image) {
        this.categoryForEdit.icon = `${image.product_id}/images/${image.slug}`;
    }
}

