import { __decorate } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import { VueNestable, VueNestableHandle } from 'vue-nestable';
const CatalogModel = require('../catalog.model').default;
const EventBus = require('@/plugins/eventBus').default;
let CatalogEditorController = class CatalogEditorController extends Core {
    constructor() {
        super(...arguments);
        this.catalog = [];
        this.formValid = true;
        this.categoryPopup = false;
        this.categoryForEdit = {
            title: '',
            children: []
        };
        this.structureForSave = [];
        this.rules = {
            required: (value) => {
                return (typeof value != 'undefined' && value !== null && value.toString().length > 0) || "Обязательное поле";
            }
        };
    }
    mounted() {
        this.getCatalog();
    }
    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res) => {
            let data = res.data;
            this.catalog = [];
            this.catalog.push({
                id: data[0].id,
                title: data[0].title,
                children: []
            });
            this.prepareCatalog(this.catalog[0].id, this.catalog[0].children, data);
        });
    }
    prepareCatalog(id, children, data) {
        data.forEach(item => {
            if (item.parent == id) {
                children.push({
                    id: item.id,
                    title: item.title,
                    children: []
                });
                this.prepareCatalog(item.id, children[children.length - 1].children, data);
            }
        });
    }
    async saveEditForm() {
        await CatalogModel
            .saveItem(this.categoryForEdit)
            .then((res) => {
            EventBus.$emit('showMessage', { ...res.data, type: 'success' });
            this.categoryPopup = false;
            this.getCatalog();
        });
    }
    categoryPopupOpened(value) {
        if (!value) {
            this.categoryForEdit = {
                title: '',
                children: []
            };
        }
    }
    openEditCategoryPopup(category = {}) {
        if (!category.id) {
            this.categoryForEdit = {
                title: '',
                children: []
            };
        }
        else {
            this.categoryForEdit = Object.assign({}, category);
        }
        this.categoryPopup = true;
    }
    async saveStructure() {
        this.structureForSave.push({
            id: this.catalog[0].id,
            title: this.catalog[0].title,
            parent: this.catalog[0].parent
        });
        this.prepareStructure(this.catalog[0].id, this.catalog[0].children);
        await CatalogModel
            .saveStructure(this.structureForSave)
            .then((res) => {
            EventBus.$emit('showMessage', { ...res.data, type: 'success' });
            this.structureForSave = [];
        });
    }
    prepareStructure(parentId, childrens) {
        if (childrens.length) {
            childrens.forEach(item => {
                let tmp = {
                    id: item.id,
                    title: item.title,
                    parent: parentId
                };
                this.structureForSave.push(tmp);
                if (item.children && item.children.length) {
                    this.prepareStructure(item.id, item.children);
                }
            });
        }
    }
};
CatalogEditorController = __decorate([
    Component({
        components: {
            VueNestable,
            VueNestableHandle
        }
    })
], CatalogEditorController);
export default CatalogEditorController;
//# sourceMappingURL=catalog.editor.controller.js.map