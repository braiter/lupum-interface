import axios from 'axios';
class CatalogModel {
    getCatalog() {
        return axios.get(`${process.env.VUE_APP_BACKEND_API}/catalog`);
    }
    saveItem(category) {
        return axios.post(`${process.env.VUE_APP_BACKEND_API}/catalog`, category);
    }
    updateItem(category) {
        return axios.put(`${process.env.VUE_APP_BACKEND_API}/catalog/${category.id}`, category);
    }
    deleteItem(category) {
        return axios.delete(`${process.env.VUE_APP_BACKEND_API}/catalog/${category.id}`, category);
    }
    saveStructure(structure) {
        return axios.post(`${process.env.VUE_APP_BACKEND_API}/catalog/saveStructure`, structure);
    }
}
export default new CatalogModel();
//# sourceMappingURL=catalog.model.js.map