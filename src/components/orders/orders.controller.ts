import Component from 'vue-class-component';
import Core from '@/components/core/core.controller.ts';
const EventBus = require('@/plugins/eventBus').default;
import { countries } from 'country-list-json';

import {mapState} from "vuex";
import {Watch} from "vue-property-decorator";
import axios from "axios";

const OrdersModel = require('@/components/orders/orders.model').default;

@Component
export default class OrdersController extends Core {
	orders: Array<any> = [];
	statuses: Array<any> = [
		{id: 1, title: 'canceled', disabled: true},
		{id: 2, title: 'order created'},
		{id: 3, title: 'order confirmed'},
		{id: 4, title: 'shipping'},
		{id: 5, title: 'delivering'},
		{id: 6, title: 'delivered'}
	];
	countries: Array<any> = countries;
	orderForEdit: any = null;
	editFormValid: boolean = false;
	openedOrder: any = null;

	mounted() {
		this.getOrders();
	}

	async getOrders() {
		this.orders = await OrdersModel
			.get();

		console.log(this.orders);
	}
	
	getOrderStatus(order) {
		return this.statuses.find(status => status.id === order.status);
	}
	
	getOrderCountry(order) {
		return this.countries.find(country => country.code === order.country);
	}
	
	cartSum(order) {
		return order.products.reduce(
			(accumulator, product) => accumulator + (this.roundNum(product.price) * product.count),
			0
		);
	}
	
	editOrder(order) {
		order.edit = true;
		this.orderForEdit = JSON.parse(JSON.stringify(order));
		
		this.$forceUpdate();
	}
	
	cancelEdit(order) {
		order.edit = false;
		this.$forceUpdate();
	}
	
	async removeProduct(index) {
		let confirm = await this.$confirm( "Do you really want to delete this product from order?", {
			buttonTrueText : "Yes",
			buttonFalseText: "Cancel"
		})
		
		if (confirm) {
			this.orderForEdit.products.splice(index, 1);
			this.$forceUpdate();
		}
	}
	
	async saveOrder() {
		EventBus.$emit('showLoader', true);
		let order = new OrdersModel(this.orderForEdit);
		await order.save();
		this.openedOrder = null;
		
		setTimeout(() => {
			EventBus.$emit('showLoader', false, 500);
			this.getOrders();
		}, 500);
	}
	
	async cancelOrder(order) {
		let confirm = await this.$confirm( "Are you sure to cancel this order?", {
			buttonTrueText : "Yes",
			buttonFalseText: "Cancel"
		})
		
		if (confirm) {
			this.orderForEdit = JSON.parse(JSON.stringify(order));
			this.orderForEdit.status = 1;
			this.saveOrder();
		}
	}
}
