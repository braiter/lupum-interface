import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import {mapState} from "vuex";

const EventBus = require('@/plugins/eventBus').default;

const WholesalesConfigModel = require('./wholesales.config.model').default;

@Component({
    computed: {
        ...mapState(['loggedUser', 'appConfig']),
        getConfig() {
            return this.$store.getters.getConfig;
        }
    }
})
export default class WholesalesConfigController extends Core {
    wholesaleConfig: any = [];
    formValid: boolean = true;
    dialog: boolean = false;
    tableHeaders: Array<object> = [
        {
            text: 'Название',
            sortable: false,
            value: 'name',
        },
        {text: '% от базового опта', value: 'percentage'},
        {text: '', value: 'actions'}
    ];

    editedItem: any = {
        name: '',
        percentage: 0
    }

    wholesaleValidField(value: any, index: any) {
        // return (!!parseInt(value) && (!this.wholesaleConfig[index - 1] || parseInt(this.wholesaleConfig[index - 1].percentage) < value)) || "Неверное поле"
        return !!parseFloat(value) || "Неверное поле"
    }

    mounted() {
        this.getWholesalesConfig();
    }

    async getWholesalesConfig() {
        let config = await WholesalesConfigModel.get();

        this.wholesaleConfig = config.filter(item => item.percentage);
    }

    async saveForm() {
        this.editedItem.save().then((res) => {
            EventBus.$emit('showMessage', {...res, type: 'success'});
            this.dialog = false;

            this.getWholesalesConfig();
        });
    }

    openPopup(item = {name: '', percentage: 0}) {
        this.dialog = true;
        this.editedItem = new WholesalesConfigModel(item);
    }
}

