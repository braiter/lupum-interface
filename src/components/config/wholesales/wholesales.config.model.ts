import Model from "@/plugins/Model";

export default class WholesalesConfigModel extends Model {
    resource() {
        return "config/wholesales"
    }
}
