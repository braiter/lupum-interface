import axios from 'axios';
const WholesalesConfigModel = require('./wholesales/wholesales.config.model').default;

class configModel {
    get() {
        let promiseStack: Array<any> = [];
    
        promiseStack.push(WholesalesConfigModel.get());
        
        return Promise.all(promiseStack);
    }
}

export default new configModel();
