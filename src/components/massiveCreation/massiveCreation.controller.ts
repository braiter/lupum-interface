import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import {mapState} from "vuex";
import axios from "axios";
import draggable from 'vuedraggable';
import {Watch} from "vue-property-decorator";
const EventBus = require('@/plugins/eventBus').default;

const CatalogModel = require('@/components/catalog/catalog.model').default;
const ProductModel = require('@/components/product/product.model').default;

@Component({
    computed: {
        ...mapState(['appConfig']),
        getConfig() {
            return this.$store.getters.getConfig;
        }
    },
    components: {
        draggable,
    }
})
export default class WholesalesConfigController extends Core {
    tableHeaders: Array<object> = [
        {
            text: 'Title RU',
            sortable: true,
            value: 'title_ru',
        },
        {
            text: 'Title EN',
            sortable: true,
            value: 'title_en',
        },
        {
            text: 'Retail price',
            sortable: false,
            value: 'retail_price',
        },
        {
            text: 'Wholesale price',
            sortable: false,
            value: 'wholesale_price',
        },
        {
            text: 'Position',
            sortable: false,
            value: 'position',
        },
        { text: '', value: 'actions'}
    ];
    
    products: Array<any> = [];
    catalog: Array<any> = [];
    flatCatalog: Array<any> = [];
    selectedCategory = [];
    
    productsLoading: boolean = false;
    
    debounceTimer: any;
    filter: string = '';
    
    mounted() {
        this.getCatalog();
    }
    
    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res: any) => {
                this.flatCatalog = res.data;
    
                this.flatCatalog.forEach(item => {
                    if (!item.parent) {
                        this.catalog.push({
                            id: item.id,
                            title_en: item.title_en,
                            children: []
                        });
                        
                        this.prepareCatalog(
                            this.catalog[this.catalog.length-1].id,
                            this.catalog[this.catalog.length-1].children,
                            res.data
                        );
                    }
                });
            });
    }
    
    prepareCatalog(id, children, arr) {
        arr.forEach(item => {
            if (item.parent == id) {
                children.push({
                    id: item.id,
                    title_en: item.title_en,
                    parent: item.parent,
                    children: []
                });
                
                this.prepareCatalog(
                    item.id,
                    children[children.length - 1].children,
                    arr
                )
            }
        })
    }
    
    get selectedCategoryTitle() {
        if (this.selectedCategory.length) {
            return this.flatCatalog.find(category => category.id == this.selectedCategory[0]).title_en
        }
        
        return '';
    }
    
    async getProducts(category) {
        this.productsLoading = true;
        this.products = [];
        
        await ProductModel
            .params({
                title: this.filter,
                category: this.selectedCategory[0]
            })
            .first()
            .then(data => {
                setTimeout(() => {
                    this.products = data.rows;
                    this.productsLoading = false;
                }, 1000);
            })
    }
    
    addNewProduct() {
        this.products.unshift({});
        
        let newItemsCount = this.products.filter(item => !item.id).length;
        
        this.products.forEach((product, index) => {
            if (!product.id) {
                product.position = this.products.length - newItemsCount + index + 1;
            }
        });
    }
    
    get validateNewProducts() {
        let validItems = this.products.filter(item =>
            !item.id && !!item.title_ru && !!item.title_en && !!item.retail_price && !!item.wholesale_price
        );
        
        let newItems = this.products.filter(item => !item.id);
        
        return validItems.length === newItems.length && !!newItems.length;
    }
    
    get validateExistingProducts() {
        return !this.products.filter(item => !item.id).length;
    }
    
    onDropCallback(item, lol, lol2) {
        let newElements = this.products.filter(product => !product.id),
            existingProducts = this.products.filter(product => !!product.id);
        
        this.products = newElements.concat(existingProducts.map((item, index) => ({...item, position: index + 1})));
    }
    
    async deleteItem(index) {
        let keys = Object.keys(this.products[index]);
        
        if (keys.filter(key => !this.products[index][key]).length == keys.length - 1) {
            this.products.splice(index, 1);
        } else {
            let confirm = await this.$confirm( "Вы уверены что хотите удалить эту запись?", {
                buttonTrueText : "Подтвердить",
                buttonFalseText: "Отмена"
            })
            
            if (confirm) {
                this.products.splice(index, 1);
            }
        }
    }
    
    async createProducts() {
        let productsForSave = this.products
            .filter(item => !item.id)
            .map(item => ({...item, category_id: this.selectedCategory}));
        
        await axios
            .post(`${process.env.VUE_APP_BACKEND_API}/products`, productsForSave)
            .then((res) => {
                EventBus.$emit('showMessage', {...res.data, type: 'success'});
    
                this.getProducts(this.selectedCategory);
            });
    }
    
    async updateProducts() {
        let productsForSave = this.products
            .filter(item => !!item.id);
        
        await axios
            .put(`${process.env.VUE_APP_BACKEND_API}/products`, productsForSave)
            .then((res) => {
                EventBus.$emit('showMessage', {...res.data, type: 'success'});
                
                this.getProducts(this.selectedCategory);
            });
    }
    
    debounceSearch() {
        // cancel pending call
        clearTimeout(this.debounceTimer);
        
        // delay new call 500ms
        this.debounceTimer = setTimeout(() => {
            this.getProducts(1);
        }, 500)
    }
    
    @Watch('filter')
    filterChanged(value: any) {
        this.debounceSearch();
    }
}


