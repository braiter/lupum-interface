import { __decorate } from "tslib";
import { Vue } from 'vue-property-decorator';
import Component from 'vue-class-component';
import { mapState } from "vuex";
import UserModel from '../user/user.model';
const EventBus = require('@/plugins/eventBus').default;
let App = class App extends Vue {
    constructor() {
        super(...arguments);
        this.showMessage = false;
        this.messageText = '';
        this.messageType = 'success';
        this.showLoader = false;
    }
    mounted() {
        EventBus.$on('showMessage', (data) => {
            this.showMessage = true;
            this.messageText = data.message;
            this.messageType = data.type || 'success';
            let timeout = data.timeout || 2000;
            setTimeout(() => {
                this.showMessage = false;
            }, timeout);
        });
        EventBus.$on('showLoader', (show, delay = 0) => {
            if (delay != 0) {
                setTimeout(() => {
                    this.showLoader = show;
                }, delay);
            }
            else {
                this.showLoader = show;
            }
        });
    }
    getLoggedUser() {
        UserModel
            .getLoggedUser()
            .then((res) => {
            this.$store
                .dispatch("setUser", res.data);
        });
    }
};
App = __decorate([
    Component({
        computed: mapState(['isUserLogged', 'loggedUser'])
    })
], App);
export default App;
//# sourceMappingURL=app.controller.js.map