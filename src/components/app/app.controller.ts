import { Vue } from 'vue-property-decorator';
import Component from 'vue-class-component';
import { mapState } from "vuex";

import UserModel from '../user/user.model';
const EventBus = require('@/plugins/eventBus').default;

@Component({
    computed: mapState(['isUserLogged', 'loggedUser'])
})

export default class App extends Vue {
    showMessage: boolean = false;
    messageText: string = '';
    messageType: string = 'success';
    showLoader: boolean = false;
    loggedUser: any;
    appConfig: any = null;

    mounted() {
        EventBus.$on('showMessage', (data) => {
            this.showMessage = true;
            this.messageText = data.message;
            this.messageType = data.type || 'success';

            let timeout = data.timeout || 2000;

            setTimeout(() => {
                this.showMessage = false;
            }, timeout);
        });

        EventBus.$on('showLoader', (
            show: boolean,
            delay: number = 0
        ) => {
            if (delay != 0) {
                setTimeout(() => {
                    this.showLoader = show;
                }, delay)
            } else {
                this.showLoader = show;
            }
        });
    }

    getLoggedUser() {
         UserModel
            .getLoggedUser()
            .then((res: any) => {
                this.$store
                    .dispatch("setUser", res.data);
            })
    }
}
