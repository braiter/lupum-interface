import { __decorate } from "tslib";
import { Vue } from 'vue-property-decorator';
import Component from 'vue-class-component';
import { mapState } from "vuex";
let core = class core extends Vue {
    constructor() {
        super(...arguments);
        this.rules = {
            required: (value) => {
                return (typeof value != 'undefined' && value !== null && value.toString().length > 0) || "Обязательное поле";
            },
            notNull: (value) => {
                return (value != 0 && value != '0.00') || 'Положительная сумма';
            }
        };
    }
    mounted() {
    }
    roundNum(num) {
        return Math.trunc(num);
    }
};
core = __decorate([
    Component({
        computed: mapState(['isUserLogged'])
    })
], core);
export default core;
//# sourceMappingURL=core.controller.js.map