import { Vue } from 'vue-property-decorator';
import Component from 'vue-class-component';
import {mapState} from "vuex";

@Component({
    computed: mapState(['isUserLogged'])
})

export default class core extends Vue {
	months: Array<string> = ['Янв', 'Фев', 'Мрт', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Нояб', 'Дек'];

    rules: any = {
        required: (value:any) => {
            return ( typeof value != 'undefined' && value !== null && value.toString().length > 0 ) || "Обязательное поле"
        },
        notNull: (value: any) => {
            return (value != 0 && value != '0.00') || 'Положительная сумма'
        }
    };

    mounted() {}

    roundNum(num: any) {
        return Math.trunc(num);
    }

	formatDateToRu(date: Date, exciseDate: boolean = false) {
		let d = new Date(date),
			month = this.months[d.getMonth()],
			day = '' + d.getDate(),
			year = d.getFullYear();

        if (exciseDate) {
            return [day, month, year].join(' ');
        }

		return [day, month, year].join(' ') + ` ${d.getHours()}:${d.getMinutes()}`;
	}

    formatHours(time: number) {
        let hours = parseInt((time / 3600).toString()),
            minutes = parseInt(((time - (hours * 3600)) / 60).toString()),
            seconds = parseInt((time - (hours * 3600) - (minutes * 60)).toString());

        return [this.addZero(hours), this.addZero(minutes), this.addZero(seconds)].join(':');
    }

    addZero(i) {
        if (i < 10) {i = "0" + i}
        return i;
    }
}
