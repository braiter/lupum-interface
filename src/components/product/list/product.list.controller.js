import { __decorate, __metadata } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import axios from "axios";
import { Watch } from 'vue-property-decorator';
import ProductItem from '@/components/product/item/product.item.vue';
import { mapState } from "vuex";
const ProductModel = require('../product.model').default;
const EventBus = require('@/plugins/eventBus').default;
let ProductController = class ProductController extends Core {
    constructor() {
        super(...arguments);
        this.productList = [];
        this.loadedImage = 0;
        this.totalPages = 0;
        this.page = 1;
        this.openedProduct = null;
        this.productModal = false;
        this.productForEdit = null;
        this.filter = '';
        this.filterLoading = false;
    }
    mounted() {
        this.getProducts(1);
        EventBus.$on('getProducts', (page) => {
            this.getProducts(page);
        });
    }
    async getProducts(page = this.page) {
        EventBus.$emit('showLoader', true);
        let filter = this.filter;
        this.openedProduct = null;
        await ProductModel
            .params({
            page,
            filter,
            category: this.$route.params.id
        })
            .first()
            .then((res) => {
            this.filterLoading = false;
            this.$vuetify.goTo(0);
            this.productList = res.rows;
            this.totalPages = res.totalPages;
            this.page = res.page;
            this.loadedImage = 0;
            this.getImages(this.productList[this.loadedImage]);
            EventBus.$emit('showLoader', false, 500);
        });
    }
    getImages(product) {
        if (product.images.length) {
            axios.get(`${process.env.VUE_APP_BACKEND_API}/product/${product.id}/images/${product.images[0]}`)
                .then((res) => {
                this.productList[this.loadedImage].image = res.data;
                this.$forceUpdate();
                if (this.loadedImage != this.productList.length - 1) {
                    this.loadedImage++;
                    this.getImages(this.productList[this.loadedImage]);
                }
            });
        }
        else if (this.loadedImage != this.productList.length - 1) {
            this.loadedImage++;
            this.getImages(this.productList[this.loadedImage]);
        }
    }
    async editProduct(product = { id: 'new' }) {
        EventBus.$emit('showLoader', true);
        this.productForEdit = JSON.parse(JSON.stringify(product));
        setTimeout(() => {
            EventBus.$emit('showLoader', false);
            EventBus.$emit('editProduct', true);
        });
    }
    debounceSearch() {
        // cancel pending call
        clearTimeout(this.debounceTimer);
        this.filterLoading = true;
        // delay new call 500ms
        this.debounceTimer = setTimeout(() => {
            this.getProducts(1);
        }, 500);
    }
    get isUserAdmin() {
        return this.loggedUser.group == 1;
    }
    addProductToCart(product) {
        EventBus.$emit('updateCart', {
            product,
            count: 1
        });
    }
    filterChanged(value) {
        this.debounceSearch();
    }
    async isAccordionOpened(value) {
        if (!!value) {
            setTimeout(() => {
                let contentOffset = document.getElementsByClassName('v-expansion-panel--active')[0]['offsetTop'], contentHeader = document.getElementsByClassName('v-expansion-panel-header--active')[0].clientHeight;
                this.$vuetify.goTo(contentOffset + contentHeader + 90);
            }, 300);
        }
    }
};
__decorate([
    Watch('filter'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ProductController.prototype, "filterChanged", null);
__decorate([
    Watch('openedProduct'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "isAccordionOpened", null);
ProductController = __decorate([
    Component({
        computed: mapState(['loggedUser']),
        components: {
            'product-item': ProductItem
        },
    })
], ProductController);
export default ProductController;
//# sourceMappingURL=product.list.controller.js.map