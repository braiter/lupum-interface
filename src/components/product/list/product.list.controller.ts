import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import axios from "axios";

import { Watch } from 'vue-property-decorator';

import ProductItem from '@/components/product/item/product.item.vue';
import {mapState} from "vuex";

const ProductModel = require('../product.model').default;
const OrdersModel = require('@/components/orders/orders.model').default;

const EventBus = require('@/plugins/eventBus').default;

@Component({
    computed: {
        ...mapState(['loggedUser', 'appConfig'])
    },
    components: {
        'product-item': ProductItem
    },
})
export default class ProductController extends Core {
    productList: Array<any> = [];
    totalPages: number = 0;
    page: number = 1;

    loggedUser: any;

    catalog: Array<any> = [];
    catalogLoading: boolean = false;
    
    productForEdit: any = null;

    debounceTimer: any;
    filter: string = '';
    filterLoading: boolean = false;
    
    imagesPopup: boolean = false;
    productDetails: any = {};

    imgPath: string = `${process.env.VUE_APP_BACKEND_API}/product`;
    showOnlyInStock: boolean = true;

    headers: Array<any> = [
        {
            text: 'Title RU',
            align: 'start',
            sortable: false,
            value: 'title_ru',
            width: '300px'
        },
        {
            text: 'Title EN',
            align: 'start',
            sortable: false,
            value: 'title_en',
            width: '300px'
        },
        { text: 'Опт', value: 'wholesale_price', sortable: false},
        { text: 'Розница', value: 'retail_price', sortable: false}
    ];

    mounted() {
        this.getProducts(1);
        this.getCatalog();

        EventBus.$on('getProducts', (page: any) => {
            this.getProducts(page);
        })
    }

    async getCatalog() {
        this.catalog = await this.$store.dispatch('getCatalog');
    }

    get categoryName() {
        if (this.catalog.length) {
            return this.catalog.find(item => item.id == this.$route.params.id);
        }

        return {title: ''};
    }

    async getProducts(page: number = this.page) {
        EventBus.$emit('showLoader', true);

        this.$store.dispatch('getConfig')
            .then(async (config) => {
                if (this.headers.length == 4) {
                    // config[0].forEach((wholesale: any, index: number) => {
                    //     if (wholesale.id > 2) {
                    //         this.headers.push({text: wholesale.name, value: `wholesale_${index-2}`, sortable: false})
                    //     }
                    // })

                    this.headers.push({ text: 'Stock', value: 'stock', sortable: false});
                    this.headers.push({ text: 'Status', value: 'status', sortable: false});
                    this.headers.push({ text: '', value: 'actions', sortable: false, width: "150px"});
                }

                await ProductModel
                    .params({
                        page,
                        in_stock: this.showOnlyInStock,
                        title: this.filter,
                        category: this.$route.params.id,
                        showAll: true
                    })
                    .first()
                    .then((res: any) => {
                        this.filterLoading = false;
            
                        this.$vuetify.goTo(0);
                        
                        let prices = JSON.parse(JSON.stringify(config[0]));
                        prices.splice(0, 2);
                        
                        let items = res.rows.map((item) => {
                            return {...item, extraWholesales: prices};
                        });
            
                        this.productList = items;
                        this.totalPages = res.totalPages;
                        this.page = page;
            
                        EventBus.$emit('showLoader', false, 500);
                    })
            });
    }
    
    async editProduct(product: any = {}) {
        this.productForEdit = JSON.parse(JSON.stringify(product));

        this.$nextTick(() => {
            EventBus.$emit('editProduct', true);
        });
    }

    debounceSearch() {
        // cancel pending call
        clearTimeout(this.debounceTimer);

        this.filterLoading = true;

        // delay new call 500ms
        this.debounceTimer = setTimeout(() => {
            this.getProducts(1);
        }, 500)
    }

    get isUserAdmin() {
        return this.loggedUser.group == 1;
    }

    // async addProductToCart(product: any) {
    //     await axios
    //         .post(`${process.env.VUE_APP_BACKEND_API}/orders/addProduct`, {
    //             product: {
    //                 ...product,
    //                 count: 1
    //             }
    //         })
    //         .then((data) => {
    //             EventBus.$emit('showMessage', {message: "Товар добавлен в корзину", type: 'success'});
    //             EventBus.$emit('getCart');
    //         })
    // }

    showProductImages(product: any) {
        this.imagesPopup = true;
        this.productDetails = product;
    }
    
    async deleteProduct(product: any) {
        let confirm = await this.$confirm( "Вы уверены что хотите удалить этот товар?", {
            buttonTrueText : "Подтвердить",
            buttonFalseText: "Отмена"
        })
    
        if (confirm) {
            product = new ProductModel(product);
            
            await product
                .delete()
                .then((res) => {
                    this.getProducts();
                    EventBus.$emit('showMessage', {message: "Успешно сохранено", type: 'success'});
                })
        }
    }

    async save(product) {
        let item = new ProductModel(product);

        EventBus.$emit('showLoader', true);

        item
            .save()
            .then(async (res: any ) => {
                setTimeout(() => {
                    EventBus.$emit('showMessage', {message: "Saved successfully", type: 'success'});
                }, 1500);

                EventBus.$emit('showLoader', false, 1500);
            });
    }

    @Watch('filter')
    filterChanged(value: any) {
        this.debounceSearch();
    }
}

