import Model from "@/plugins/Model";
export default class ProductModel extends Model {
    resource() {
        return "product";
    }
}
//# sourceMappingURL=product.model.js.map