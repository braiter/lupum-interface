import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';

import { Prop, Watch } from 'vue-property-decorator';

import Catalog from '@/components/catalog/list/catalog.list.vue';
import UploadImage from "@/components/directives/uploadImage/uploadImage.vue";
import axios from "axios";

const ProductModel = require('../product.model').default;
const SuppliersModel = require('@/components/warehouse/suppliers/warehouse.suppliers.model').default;
const ImageModel = require("@/components/directives/uploadImage/uploadImage.model").default;
const CatalogModel = require('../../catalog/catalog.model').default;

const EventBus = require('@/plugins/eventBus').default;


@Component({
    components: {
        'catalog': Catalog,
        'upload-image': UploadImage
    }
})
export default class ProductItemController extends Core {
    @Prop() product: any;

    formValid: boolean = true;
    suppliers: Array<any> = [];
    editProductCategoryId: any = null;

    productModal: boolean = false;
    productModalTab: number = 1;

    catalog: Array<any> = [];
    flatCatalog: Array<any> = [];

    mounted() {
        this.getSuppliers();
        this.getCatalog();

        EventBus.$on('uploadFile', (data: any) => {
            this.product.image = data;
            this.$forceUpdate();
        });

        EventBus.$on('editProduct', (show: any) => {
            this.productModal = true;
        })
    }

    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res: any) => {
                let data = res.data;

                this.catalog = [];
                this.flatCatalog = data;

                if (data.length) {
                  this.catalog.push({
                    id: data[0].id,
                    title: data[0].title,
                    children: []
                  });

                  this.prepareCatalog(
                    this.catalog[0].id,
                    this.catalog[0].children,
                    data
                  );
                }
            });
    }

    prepareCatalog(
        id: any,
        children: Array<any>,
        data: Array<any>
    ) {
        data.forEach(item => {
            if (item.parent == id) {
                children.push({
                    id: item.id,
                    title: item.title,
                    children: []
                });

                this.prepareCatalog(
                    item.id,
                    children[children.length - 1].children,
                    data
                )
            }
        })
    }

    async getSuppliers() {
        await SuppliersModel
            .get()
            .then((res: any) => {
                this.suppliers = res;
            })
    }

    selectCategory(id: any) {
        this.product.category_id = id;
        this.$forceUpdate();
    }

    saveProduct() {
        EventBus.$emit('showLoader', true);

        let images = this.product.images;

        delete this.product.images;

        let item = new ProductModel({
            ...this.product
        });

        item
            .save()
            .then(async (res: any ) => {
                this.productModal = false;

                await this.uploadImages(res.item, images).then(() => {
                    EventBus.$emit('showMessage', {message: "Успешно сохранено", type: 'success'});
                    EventBus.$emit('showLoader', false, 1500);
                    EventBus.$emit('getProducts');
                });
            });
    }

    async uploadImages(obj: any, images: Array<any>) {
        let form = new FormData();

        images.forEach((image) => {
            if (image.binaryFile) {
                image.binaryFile.position = image.position;
                form.append('filesForSave', image.binaryFile);
            }
            else if (image.slug) {
                form.append('savedFiles', JSON.stringify(image));
            }

        });

        return axios.post(`${process.env.VUE_APP_BACKEND_API}/product/${obj.id}/images`, form, {
            headers: {
                "Content-Type": "multipart/form-data"
            }})
    }

    getCaregoryById(id: any) {
        let filterResults = this.flatCatalog.filter(item => item.id == id);

        if(!filterResults.length) {
            return {id: 0, title: ''};
        }
        return filterResults[0];
    }

    validateForm() {
        if (this.productModalTab === 1) {
            this.$refs.form['validate']();

            if (this.formValid) {
                this.productModalTab++;
            }
        } else if (this.productModalTab < 3) {
            this.productModalTab++;
        } else {
            this.saveProduct();
        }
        
    }

    @Watch("productModal")
    WatchProductModal(value: boolean) {
        if (value == false) {


            if (this.$refs.form) {
                this.$refs.form['resetValidation']();
            }
            this.productModalTab = 1;
        }
    }
}

