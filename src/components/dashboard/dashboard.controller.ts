import Component from 'vue-class-component';
import { FCComponent } from 'vue-fusioncharts';
import Core from '@/components/core/core.controller';
import ProductList from '@/components/product/list/product.list.vue';
import {mapState} from "vuex";
import FusionCharts from 'fusioncharts';
import axios from "axios";
import progressBar from "@/components/directives/progressBar/progressBar.vue";

@Component({
    components: {
        'progress-bar': progressBar
    },
    computed: mapState(['isUserLogged'])
})
export default class DashboardController extends Core {
    width =  "100%";
    height = "400";
    type = "timeseries";
    dataFormat = "json";
    progress: number = 0;
    progressHeight: number = 0;
    progressWidth: number = 100;
    dataSource: any = {
        data: null,
        caption: {
            text: "Sales Analysis"
        },
        subcaption: {
            text: "Grocery"
        },
        yAxis: [
            {
                plot: {
                    value: "Grocery Sales Value",
                    type: "line"
                },
                format: {
                    prefix: "$"
                },
                title: "Sale Value"
            }
        ]
    };
    schema = [
        {
            "name": "Time",
            "type": "date",
            "format": "%d-%b-%y"
        },
        {
            "name": "Grocery Sales Value",
            "type": "number"
        }
    ];
    
    data = [
        [
            "01-Feb-22",
            8866
        ],
        [
            "02-Feb-22",
            2174
        ],
        [
            "03-Feb-22",
            2084
        ],
        [
            "04-Feb-22",
            1503
        ],
        [
            "05-Feb-22",
            4928
        ],
        [
            "06-Feb-22",
            4667
        ],
        [
            "07-Feb-22",
            1064
        ],
        [
            "08-Feb-22",
            851
        ],
        [
            "09-Feb-22",
            7326
        ],
        [
            "10-Feb-22",
            8399
        ],
        [
            "11-Feb-22",
            4084
        ],
        [
            "12-Feb-22",
            4012
        ],
        [
            "13-Feb-22",
            1673
        ],
        [
            "14-Feb-22",
            6018
        ],
        [
            "15-Feb-22",
            9011
        ],
        [
            "16-Feb-22",
            5817
        ],
        [
            "17-Feb-22",
            5813
        ],
        [
            "18-Feb-22",
            566
        ],
        [
            "19-Feb-22",
            9065
        ],
        [
            "20-Feb-22",
            6734
        ],
        [
            "21-Feb-22",
            6937
        ],
        [
            "22-Feb-22",
            3038
        ],
        [
            "23-Feb-22",
            4445
        ],
        [
            "24-Feb-22",
            8782
        ],
        [
            "25-Feb-22",
            9489
        ],
        [
            "26-Feb-22",
            9624
        ],
        [
            "27-Feb-22",
            9033
        ],
        [
            "28-Feb-22",
            6932
        ],
        [
            "01-Mar-22",
            625
        ],
        [
            "02-Mar-22",
            904
        ],
        [
            "03-Mar-22",
            7572
        ],
        [
            "04-Mar-22",
            2124
        ],
        [
            "05-Mar-22",
            8157
        ],
        [
            "06-Mar-22",
            8666
        ],
        [
            "07-Mar-22",
            3186
        ],
        [
            "08-Mar-22",
            9247
        ],
        [
            "09-Mar-22",
            8393
        ],
        [
            "10-Mar-22",
            9191
        ],
        [
            "11-Mar-22",
            1278
        ],
        [
            "12-Mar-22",
            7252
        ],
        [
            "13-Mar-22",
            6516
        ],
        [
            "14-Mar-22",
            8176
        ],
        [
            "15-Mar-22",
            5759
        ],
        [
            "16-Mar-22",
            7601
        ],
        [
            "17-Mar-22",
            3616
        ],
        [
            "18-Mar-22",
            9612
        ],
        [
            "19-Mar-22",
            2631
        ],
        [
            "20-Mar-22",
            4939
        ],
        [
            "21-Mar-22",
            3578
        ],
        [
            "22-Mar-22",
            3742
        ],
        [
            "23-Mar-22",
            5100
        ],
        [
            "24-Mar-22",
            6925
        ],
        [
            "25-Mar-22",
            5239
        ],
        [
            "26-Mar-22",
            1461
        ],
        [
            "27-Mar-22",
            2317
        ],
        [
            "28-Mar-22",
            8238
        ],
        [
            "29-Mar-22",
            3617
        ],
        [
            "30-Mar-22",
            8680
        ],
        [
            "31-Mar-22",
            8487
        ],
        [
            "01-Apr-22",
            1768
        ],
        [
            "02-Apr-22",
            1227
        ],
        [
            "03-Apr-22",
            4137
        ],
        [
            "04-Apr-22",
            9055
        ],
        [
            "05-Apr-22",
            4984
        ],
        [
            "06-Apr-22",
            1577
        ],
        [
            "07-Apr-22",
            6624
        ],
        [
            "08-Apr-22",
            5802
        ],
        [
            "09-Apr-22",
            5190
        ],
        [
            "10-Apr-22",
            3092
        ],
        [
            "11-Apr-22",
            7304
        ],
        [
            "12-Apr-22",
            3445
        ],
        [
            "13-Apr-22",
            1480
        ],
        [
            "14-Apr-22",
            3705
        ],
        [
            "15-Apr-22",
            7795
        ],
        [
            "16-Apr-22",
            7405
        ],
        [
            "17-Apr-22",
            3901
        ],
        [
            "18-Apr-22",
            4428
        ],
        [
            "19-Apr-22",
            8874
        ],
        [
            "20-Apr-22",
            3072
        ],
        [
            "21-Apr-22",
            6340
        ],
        [
            "22-Apr-22",
            2275
        ],
        [
            "23-Apr-22",
            8934
        ],
        [
            "24-Apr-22",
            7595
        ],
        [
            "25-Apr-22",
            3869
        ],
        [
            "26-Apr-22",
            7395
        ],
        [
            "27-Apr-22",
            7812
        ],
        [
            "28-Apr-22",
            9642
        ],
        [
            "29-Apr-22",
            6925
        ],
        [
            "30-Apr-22",
            8537
        ],
        [
            "01-May-22",
            2985
        ]
    ]
    
    mounted() {
        const fusionTable = new FusionCharts.DataStore().createDataTable(
            this.data,
            this.schema
        );

        this.dataSource.data = fusionTable;
    
        axios.get(`${process.env.VUE_APP_BACKEND_API}/statistics/stocks`)
            .then(data => {
                console.log(data);
            })
    }
}

