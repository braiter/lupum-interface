import { __decorate } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import ProductList from '@/components/product/list/product.list.vue';
import { mapState } from "vuex";
let DashboardController = class DashboardController extends Core {
    mounted() {
    }
};
DashboardController = __decorate([
    Component({
        components: {
            'product-list': ProductList
        },
        computed: mapState(['isUserLogged'])
    })
], DashboardController);
export default DashboardController;
//# sourceMappingURL=dashboard.controller.js.map