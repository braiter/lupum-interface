import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import axios from 'axios';
import UserModel from '../user.model'
import {mapGetters, mapState} from "vuex";

const EventBus = require('@/plugins/eventBus').default;

@Component({
    computed: {
        ...mapState(['loggedUser']),
        getLoggedUser()  {
            return this.$store.getters.getLoggedUser;
        }
    }
})
export default class UserRegisterController extends Core {
    user: object = {
        email: '',
        password: '',
        name: ''
    };

    loggedUser: any;

    id: any = this.$route.params.id || null;
    formValid: boolean = true;
    groups: Array<Object> = [];
    isLoading: boolean = false;

    rules: any = {
        required: (value:any) => {
            return ( typeof value != 'undefined' && value !== null && value.toString().length > 0 ) || "Обязательное поле"
        }
    };

    mounted() {
        this.getGroups();

        if (this.id) {
            if (this.id == 'me') {

                this['getLoggedUser'].then((res: any) => {
                    this.user = res;
                })
            }
            else {
                this.getProfile();
            }
        }
    }

    async getProfile() {
        await UserModel
            .getProfile({id: this.id})
            .then((res: any) => {
                this.user = res.data[0];
            })
    }

    getGroups() {
        this.isLoading = true;
        UserModel
          .getGroups()
          .then((res: any) => {
              this.isLoading = false;
              this.groups = res.data;
          })
    }

    saveUser() {
        this.$refs.form['validate']();

        if (this.formValid) {
            UserModel
                .save(this.user)
                .then(
                    (res: any) => {
                        EventBus.$emit('showMessage', {...res.data, type: 'success'});

                        if (this.id === "me") {
                            localStorage.setItem('token', res.data.token);

                            this.$store
                                .dispatch("setUser", res.data.user);
                        }
                    },
                    (err: any) => {
                        EventBus.$emit('showMessage', {...err.response.data, type: 'error'})
                    }
                );
        }
    }
}

