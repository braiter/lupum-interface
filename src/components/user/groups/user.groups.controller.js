import { __decorate } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
const EventBus = require('@/plugins/eventBus').default;
let UserRegisterController = class UserRegisterController extends Core {
    constructor() {
        super(...arguments);
        this.user = {
            login: '',
            password: '',
            name: ''
        };
    }
    mounted() {
    }
};
UserRegisterController = __decorate([
    Component
], UserRegisterController);
export default UserRegisterController;
//# sourceMappingURL=user.groups.controller.js.map