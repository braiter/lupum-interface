import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import {mapState} from "vuex";

const UserModel = require('../user.model').default;

const EventBus = require('@/plugins/eventBus').default;

@Component({
    computed: mapState(['loggedUser'])
})
export default class ProductController extends Core {
    userList: Array<any> = [];
    groupList: Array<any> = [];
    filter: string = '';
    isUserAdmin: boolean = false;
    loggedUser: any;

    mounted() {
        this.getUsers();
        this.getGroups();

        this.isUserAdmin = this.loggedUser.group == 1;
    }

    async getUsers() {
        await UserModel
          .getUserList()
          .then((res: any) => {
              this.userList = res.data;
          })
    }

    async getGroups() {
        await UserModel
            .getGroups()
            .then((res: any) => {
                this.groupList = res.data;
            })
    }

    getGroupById(groupId: any) {
        return this.groupList.filter(item => item.id == groupId)[0].title;
    }

    editUser(id: any) {
        this.$router.push(`/user/profile${id? `/$id}`: '' }`);
    }
}

