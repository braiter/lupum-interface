import { __decorate } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import { mapState } from "vuex";
const UserModel = require('../user.model').default;
const EventBus = require('@/plugins/eventBus').default;
let ProductController = class ProductController extends Core {
    constructor() {
        super(...arguments);
        this.userList = [];
        this.groupList = [];
        this.filter = '';
        this.isUserAdmin = false;
    }
    mounted() {
        this.getUsers();
        this.getGroups();
        this.isUserAdmin = this.loggedUser.group == 1;
    }
    async getUsers() {
        await UserModel
            .getUserList()
            .then((res) => {
            this.userList = res.data;
        });
    }
    async getGroups() {
        await UserModel
            .getGroups()
            .then((res) => {
            this.groupList = res.data;
        });
    }
    getGroupById(groupId) {
        return this.groupList.filter(item => item.id == groupId)[0].title;
    }
    editUser(id) {
        this.$router.push(`/user/profile${id ? '/' + id : ''}`);
    }
};
ProductController = __decorate([
    Component({
        computed: mapState(['loggedUser'])
    })
], ProductController);
export default ProductController;
//# sourceMappingURL=user.list.controller.js.map