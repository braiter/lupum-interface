import axios from 'axios'

class userModel {
    login(user: any) {
        return axios.post(`${process.env.VUE_APP_BACKEND_API}/user/login`, user)
    }

    getLoggedUser() {
        return axios.get(`${process.env.VUE_APP_BACKEND_API}/user/auth`,)
    }

    getGroups() {
        return axios.get(`${process.env.VUE_APP_BACKEND_API}/user/groups`)
    }

    save(user: any) {
        return axios.post(`${process.env.VUE_APP_BACKEND_API}/user/save`, user)
    }

    getProfile(user: any) {
        return axios.get(`${process.env.VUE_APP_BACKEND_API}/user/profile?`, {params: user})
    }

    getUserList() {
        return axios.get(`${process.env.VUE_APP_BACKEND_API}/user`)
    }

    logout(user: any) {
        return axios.get(`${process.env.VUE_APP_BACKEND_API}/user/logout`)
    }
}

export default new userModel();
