import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import UserModel from "@/components/user/user.model";

import store from '@/store';
const EventBus = require('@/plugins/eventBus').default;

@Component
export default class UserLoginController extends Core {
    user: object = {
        email: '',
        password: ''
    };

    mounted() {

    }

    authorize() {
        UserModel
          .login(this.user)
          .then((res: any) => {
              localStorage.setItem('token', res.data.token);
              localStorage.setItem("tokenRegistered", (Date.now() / 1000).toString());

              this.$router.push('/dashboard')
          }).catch((err: any) => {

        });
    }
}

