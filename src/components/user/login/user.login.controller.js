import { __decorate } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import UserModel from "@/components/user/user.model";
const EventBus = require('@/plugins/eventBus').default;
let UserLoginController = class UserLoginController extends Core {
    constructor() {
        super(...arguments);
        this.user = {
            email: '',
            password: ''
        };
    }
    mounted() {
    }
    authorize() {
        UserModel
            .login(this.user)
            .then((res) => {
            localStorage.setItem('token', res.data.token);
            this.$router.push('/dashboard');
        }).catch((err) => {
        });
    }
};
UserLoginController = __decorate([
    Component
], UserLoginController);
export default UserLoginController;
//# sourceMappingURL=user.login.controller.js.map