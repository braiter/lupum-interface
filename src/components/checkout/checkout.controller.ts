import Component from 'vue-class-component';
import Core from '@/components/core/core.controller.ts';
const EventBus = require('@/plugins/eventBus').default;
import { countries } from 'country-list-json';

import {mapState} from "vuex";
import {Watch} from "vue-property-decorator";
import axios from "axios";

const OrdersModel = require('@/components/orders/orders.model').default;
import UserModel from "@/components/user/user.model";

@Component({
    computed: {
        ...mapState(['loggedUser']),
        getLoggedUser()  {
            return this.$store.getters.getLoggedUser;
        }
    }
})

export default class CheckoutController extends Core {
    loggedUser: any;
    order: any = null;
    step: number = 1;

    successSteps: object = {
        first: false,
        second: false,
        third: false
    };
    
    countries: Array<any> = countries;
    
    salesType: Array<any> = [];
    
    paymentOption : Array<any> = [
        {id: 'prepay', title: 'Prepay'},
        {id: 'pod', title: 'Payment on delivery'}
    ];

    success: boolean = false;


    mounted() {
        this.getCart();
        this.$store.dispatch('getConfig')
            .then(async (config) => {
                this.salesType = config[0];
            });
    }

    async getUserProfile() {
        await UserModel
            .getProfile({id: this.loggedUser.id})
            .then((res: any) => {
                this.order.first_name = this.order.first_name || res.data.first_name;
                this.order.last_name = this.order.last_name || res.data.last_name;
                this.order.phone = this.order.phone || res.data.phone;
            })
    }

    async getCart() {
        EventBus.$emit('showLoader', true);

        this.order = await OrdersModel
            .params({
				status: 0
            })
            .first();

		EventBus.$emit('showLoader', false, 1000);

		if (!this.order.products.length) {
			this.$router.push('dashboard')
		}

		this['getLoggedUser']
			.then(() => {
				this.getUserProfile();
			})

    }

    async confirmOrder() {
		await axios
			.post(`${process.env.VUE_APP_BACKEND_API}/orders/${this.order.id}/confirm`, this.order);
   
		this.success = true;
        EventBus.$emit('getCart');
    }
    
    get cartSum() {
        return this.order.products.reduce(
            (accumulator, product) => accumulator + (this.roundNum(product.price) * product.count),
            0
        );
    }
    
    updatePrices() {
        if (this.order.price_id == 1) {
            this.order.products.forEach(product => {
                product.price = product.details.wholesale_price;
            });
        } else if (this.order.price_id == 2) {
            this.order.products.forEach(product => {
                product.price = product.details.retail_price;
            });
        } else {
            let price = this.salesType.find(type => type.id === this.order.price_id);
            
            this.order.products.forEach(product => {
                product.price = this.roundNum(product.details.wholesale_price * (1 + parseFloat(price.percentage)/100));
            });
        }
    }
    
    async removeProduct(index) {
        let confirm = await this.$confirm( "Do you really want to delete this product from cart?", {
            buttonTrueText : "Yes",
            buttonFalseText: "Cancel"
        })
        
        if (confirm) {
            EventBus.$emit('showLoader', true);
            
            await axios
                .delete(`${process.env.VUE_APP_BACKEND_API}/orders/${this.order.id}/removeProduct/${this.order.products[index].id}`);
            this.order.products.splice(index, 1);
    
            EventBus.$emit('showLoader', false, 500);
            if (!this.order.products.length) {
                this.$router.push('dashboard');
            }
    
            EventBus.$emit('getCart');
        }
    }
}
