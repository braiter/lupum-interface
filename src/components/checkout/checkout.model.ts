import Model from "@/plugins/Model";

export default class OrdersModel extends Model {
    resource() {
        return "orders"
    }
}
