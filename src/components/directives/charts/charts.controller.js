import { __decorate, __metadata } from "tslib";
import Component, { mixins } from 'vue-class-component';
import { Prop } from 'vue-property-decorator';
import { Line } from 'vue-chartjs';
import Core from '@/components/core/core.controller';
const EventBus = require('@/plugins/eventBus').default;
let ChartsController = class ChartsController extends mixins(Core, Line) {
    mounted() {
        this.renderChart(this.chartData, { responsive: true, maintainAspectRatio: false });
        EventBus.$on('renderChat', data => this.renderChart(data, { responsive: true, maintainAspectRatio: false }));
    }
};
__decorate([
    Prop(),
    __metadata("design:type", Object)
], ChartsController.prototype, "chartData", void 0);
ChartsController = __decorate([
    Component
], ChartsController);
export default ChartsController;
//# sourceMappingURL=charts.controller.js.map