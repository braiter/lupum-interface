import Component, {mixins} from 'vue-class-component';
import { Prop } from 'vue-property-decorator';
import { Line } from 'vue-chartjs';
import Core from '@/components/core/core.controller';

const EventBus = require('@/plugins/eventBus').default;

@Component
export default class ChartsController extends mixins(Core, Line) {
    @Prop() chartData: any;


    mounted() {
        this.renderChart(this.chartData, {responsive: true, maintainAspectRatio: false});

        EventBus.$on('renderChat', data => this.renderChart(data, {responsive: true, maintainAspectRatio: false}))
    }
}
