import { __decorate, __metadata } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import { Prop } from 'vue-property-decorator';
const EventBus = require('@/plugins/eventBus').default;
let UploadImageController = class UploadImageController extends Core {
    constructor() {
        super(...arguments);
        this.activeImage = null;
        this.fileType = /pdf|png|jpeg|tiff|bmp|jpg$/g;
        this.file = null;
    }
    mounted() {
        if (this.entity.image) {
            this.activeImage = { link: this.entity.image };
        }
    }
    openUploadFileModal() {
        this.$refs.file['click']();
    }
    prepareNewFiles() {
        this.file = Object.values(this.$refs.file['files'])[0];
        if (this.file.name.toLowerCase().match(this.fileType).length) {
            this.makeBase64FromFile(this.file);
        }
        else {
            EventBus.$emit('showMessage', { message: 'Не верный тип файла', type: 'error' });
        }
    }
    makeBase64FromFile(file, isMobile = false) {
        let reader = new FileReader();
        reader.onload = (e) => {
            this.activeImage = {
                title: file.name,
                link: e.target.result,
                binaryFile: file
            };
            EventBus.$emit('uploadFile', {
                binaryFile: file
            });
            this.$forceUpdate();
        };
        reader.readAsDataURL(file);
    }
};
__decorate([
    Prop(),
    __metadata("design:type", Object)
], UploadImageController.prototype, "entity", void 0);
UploadImageController = __decorate([
    Component
], UploadImageController);
export default UploadImageController;
//# sourceMappingURL=uploadImage.controller.js.map