import Model from "@/plugins/Model";
export default class UploadImageModel extends Model {
    resource() {
        return "images";
    }
    config() {
        return {
            method: 'POST',
            header: { 'Content-Type': 'multipart/form-data' },
        };
    }
}
//# sourceMappingURL=uploadImage.model.js.map