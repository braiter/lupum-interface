import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import { Prop } from 'vue-property-decorator';
import { VueNestable, VueNestableHandle } from 'vue-nestable'

const EventBus = require('@/plugins/eventBus').default;


@Component({
    components: {
        VueNestable,
        VueNestableHandle
    }
})
export default class UploadImageController extends Core {
    activeImage: any = null;
    fileType: any = /pdf|png|jpeg|tiff|bmp|jpg|webp$/g;
    files: Array<any> = [];
    imgPath: string = `${process.env.VUE_APP_BACKEND_API}/product`;

    @Prop() entity: any;

    mounted() {
        this.entity.images.map((item, index) => ({...item, id: index}));
    }

    openUploadFileModal() {
        this.$refs.file['click']();
    }

    prepareNewFiles() {
        this.entity.images = this.entity.images || [];

        this.files = Object.values( this.$refs.file['files'] );

        this.files.forEach((file) => {
            if ( file.name.toLowerCase().match( this.fileType ).length ) {
                this.makeBase64FromFile(file);
            }
            else {
                EventBus.$emit('showMessage', {message: `Не верный тип файла: ${file.name}`, type: 'error'});
            }
        });
    }

    makeBase64FromFile( file: any, isMobile: boolean = false ) {
        let reader = new FileReader();

        reader.onload = ( e: any ) => {
            this.entity.images.unshift({
                title       : file.name,
                link        : e.target.result,
                binaryFile  : file
            });

            this.entity.images = this.entity.images.map((item, index) => ({...item, position: index}));

            this.$forceUpdate();
        };

        reader.readAsDataURL( file );
    }

    async removeImage(index: number) {
        this.entity.images.splice(index, 1);

        this.entity.images = this.entity.images.map((item, index) => ({...item, position: index}));
    }

    sortList(list) {
        this.entity.images = list.map((item, index) => ({...item, position: index}));
    }
}
