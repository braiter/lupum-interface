import Model from "@/plugins/Model";

export default class UploadImageModel extends Model {
    resource() {
        return "images"
    }

    // Override default query parameter names
    parameterNames() {
        const defaultParams = super.parameterNames();
        const customParams = {
            include: 'include_custom'
        }

        return { ...defaultParams, ...customParams }
    }

    // Configure object-to-formadata
    formData() {
        return {
            indices: true
        }
    }
}
