import Component from 'vue-class-component';
import Core from '@/components/core/core.controller.ts';
const EventBus = require('@/plugins/eventBus').default;

import MenuModel from './menu.model';
import {mapState} from "vuex";
import axios from "axios";
const OrdersModel = require('@/components/orders/orders.model').default;

@Component({
    computed: {...mapState(['isUserLogged', 'loggedUser'])}
})

export default class InnerPageController extends Core {
    mainMenuItems: Array<any> = [];
    mainMenuOpened: boolean = false;

    locales: Array<string> = ["en", "ua", "gr", "ru"]

    cart: any = null;
    cartModal: boolean = false;

    loggedUser: any;
    sessionExpiredIn: number = 0;

    mounted() {
        this.getMenu();
        this.getSessionExpired();
        // this.getCart();

        if (!Object.values(this.loggedUser).length) {
            this.$parent.$parent['getLoggedUser']();
        }

        // EventBus.$on('getCart', (data) => {
        //     this.getCart();
        // })
    }

    async getMenu() {
        this.mainMenuItems = await MenuModel
            .get();
    }
    //
    // async getCart() {
    //      await OrdersModel
    //         .params({
    //             status: 0
    //         })
    //         .get()
    //         .then((res: any) => {
    //             if (!res.length) {
    //                 new OrdersModel({})
    //                     .save()
    //                     .then((cart: any) => {
    //                         this.cart = cart;
    //                     });
    //             } else {
    //                 this.cart = res[0];
    //             }
    //         });
    // }

    getImages(product: any) {
        // let loadedImage = 0;
		//
        // if ( product.images.length ) {
        //     axios.get(`${process.env.VUE_APP_BACKEND_API}/product/${product.product_id}/images/${product.images[0]}`)
        //         .then((res: any) => {
        //             this.cart.products[loadedImage].image = res.data;
        //             this.$forceUpdate();
		//
        //             if ( loadedImage != this.cart.products.length - 1 ) {
        //                 loadedImage++;
        //                 this.getImages(this.cart.products[loadedImage]);
        //             }
        //         });
        // }
    }

    get cartSum() {
        return this.cart.products.reduce(
            (accumulator, product) => accumulator + (this.roundNum(product.price) * product.count),
            0
        );
    }

    async deleteProductFromCart(product: any) {
        await this.$confirm( "Вы уверены что хотите удалить товар из корзины?", {
            buttonTrueText : "Подтвердить",
            buttonFalseText: "Отмена"
        }).then(() => {

        })
        .finally(() => {
            setTimeout(() => {
                this.cartModal = true;
            }, 0);
        });
    }
    
    checkout() {
        this.$router.push("/checkout");
        this.cartModal = false;
    }

    getSessionExpired() {
        setInterval(() => {
            let now = Date.now() / 1000;
            let offset = 6 * 60 * 60;
            let sessionStarted = localStorage.tokenRegistered;

            this.sessionExpiredIn = (offset + parseInt(sessionStarted)) - now;


        }, 1000);
    }
}
