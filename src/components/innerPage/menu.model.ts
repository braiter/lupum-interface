import Model from "@/plugins/Model";

class MenuModel extends Model {
    resource() {
        return "menu"
    }
}

export default new MenuModel();
