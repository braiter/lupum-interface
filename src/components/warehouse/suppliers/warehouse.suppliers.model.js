import Model from "@/plugins/Model";
export default class WarehouseSuppliersModel extends Model {
    resource() {
        return "warehouse/suppliers";
    }
}
//# sourceMappingURL=warehouse.suppliers.model.js.map