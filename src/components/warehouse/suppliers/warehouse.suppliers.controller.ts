import Component from 'vue-class-component';
import axios from 'axios';
import {mapState} from "vuex";
import {Watch} from "vue-property-decorator";

import Core from '@/components/core/core.controller';

const SuppliersModel = require('@/components/warehouse/suppliers/warehouse.suppliers.model').default;
const ProductModel = require('@/components/product/product.model').default;
const CatalogModel = require('@/components/catalog/catalog.model').default;

const EventBus = require('@/plugins/eventBus').default;

@Component({
    computed: mapState(['loggedUser'])
})
export default class WarehouseSuppliersController extends Core {
    suppliers: Array<any> = [];
    creationModal: boolean = false;
    supplierForEdit: any = {};
    selectedSupplier: null | number = null;

    supplierFormValid: boolean = true;

    products: Array<any> = [];
    productsLoading: boolean = false;
    selectedProducts: Array<any> = [];

    catalog: Array<any> = [];
    flatCatalog: Array<any> = [];
    selectedCategories: Array<any> = [];
    
    filter: string = '';
    
    headers : Array<any> = [
        { text: 'Title RU', value: 'title_ru' },
        { text: 'Title EN', value: 'title_en' }
    ];

    mounted() {
        this.getSuppliers();
        this.getCatalog();
    }

    async getSuppliers() {
        await SuppliersModel
            .get()
            .then((res: any) => {
                this.suppliers = res;
            });
    }
    
    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res: any) => {
                this.flatCatalog = res.data;
                
                this.flatCatalog.forEach(item => {
                    if (!item.parent) {
                        this.catalog.push({
                            id: item.id,
                            title: item.title,
                            children: []
                        });
                        
                        this.prepareCatalog(
                            this.catalog[this.catalog.length-1].id,
                            this.catalog[this.catalog.length-1].children,
                            res.data
                        );
                    }
                });
            });
    }
    
    prepareCatalog(id, children, arr) {
        arr.forEach(item => {
            if (item.parent == id) {
                children.push({
                    id: item.id,
                    title: item.title,
                    parent: item.parent,
                    children: []
                });
                
                this.prepareCatalog(
                    item.id,
                    children[children.length - 1].children,
                    arr
                )
            }
        })
    }

    async getProducts() {
        this.productsLoading = true;
        
        await ProductModel
            .params({
                categories_list: this.selectedCategories.length? this.selectedCategories : [0]
            })
            .first()
            .then((res: any) => {
                setTimeout(() => {
                    this.productsLoading = false;
    
                    this.$vuetify.goTo(0);
    
                    this.products = res.rows;
                    
                    this.selectedProducts = this.products.filter(product => product.supplier_id === this.selectedSupplier);
                }, 1000);
            })
    }
    
    async saveSelectedProducts() {
        this.products.forEach(product => {
            if (this.selectedProducts.filter(item => item.id === product.id).length) {
                product.supplier_id = this.selectedSupplier;
            }
        });
        
        await axios
            .put(`${process.env.VUE_APP_BACKEND_API}/products`, this.products)
            .then((res) => {
                EventBus.$emit('showMessage', {...res.data, type: 'success'});
                
                this.getProducts();
            });
    }

    openCreationModal(
        supplier: any = {title: '', description: ''}
    ) {
        if (this.$refs.form) {
            this.$refs.form['resetValidation']();
        }

        this.supplierForEdit = new SuppliersModel(JSON.parse(JSON.stringify(supplier)));
        this.creationModal   = true;
    }

    saveSupplier() {
        EventBus.$emit('showLoading', true);
        this.supplierForEdit
            .save()
            .then((res: any) => {
                this.creationModal = false;
                EventBus.$emit('showLoader');

                setTimeout(() => {
                    EventBus.$emit('showMessage', {...res, type: 'success'});
                    this.getSuppliers();
                }, 1000);
            })
    }
    
    selectSupplier(id) {
        this.selectedSupplier = id;
        this.products = [];
    
        this.getSupplierCategories()
    }
    
    async getSupplierCategories() {
        await axios
            .get(`${process.env.VUE_APP_BACKEND_API}/warehouse/suppliers/${this.selectedSupplier}/categories`)
            .then((res) => {
                this.selectedCategories = res.data;
            });
    }

    async deleteSupplier(supplier: any) {
        const answer = await this.$confirm( "Вы уверены что хотите удалить поставщика?", {
            buttonTrueText : "Подтвердить",
            buttonFalseText: "Отмена"
        } );

        if ( answer ) {
            supplier
                .delete()
                .then((res: any) => {
                    this.getSuppliers();
                });
        }
    }
    
    get supplierName() {
        return this.suppliers.find(supplier => supplier.id === this.selectedSupplier).title;
    }
    
    @Watch('selectedProducts')
    selectedProductsChanged(newValue: any, oldValue: any) {
        if (oldValue.length > newValue.length) {
            oldValue.forEach(oldProduct => {
                let match = newValue.filter(newProduct => newProduct.id === oldProduct.id);
                
                if (!match.length) {
                    this.products.forEach(product => {
                        if (product.id === oldProduct.id) {
                            product.supplier_id = 0;
                        }
                    })
                }
            });
        }
    }
}

