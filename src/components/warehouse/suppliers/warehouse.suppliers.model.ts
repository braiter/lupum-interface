import Model from "@/plugins/Model";

export default class WarehouseSuppliersModel extends Model {
    resource() {
        return "warehouse/suppliers"
    }
}
