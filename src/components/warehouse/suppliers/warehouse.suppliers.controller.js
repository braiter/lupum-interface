import { __decorate, __metadata } from "tslib";
import Component from 'vue-class-component';
import axios from 'axios';
import { mapState } from "vuex";
import { Watch } from "vue-property-decorator";
import Core from '@/components/core/core.controller';
const SuppliersModel = require('@/components/warehouse/suppliers/warehouse.suppliers.model').default;
const ProductModel = require('@/components/product/product.model').default;
const CatalogModel = require('@/components/catalog/catalog.model').default;
const EventBus = require('@/plugins/eventBus').default;
let WarehouseSuppliersController = class WarehouseSuppliersController extends Core {
    constructor() {
        super(...arguments);
        this.suppliers = [];
        this.creationModal = false;
        this.supplierForEdit = {};
        this.supplierFormValid = true;
        this.searchProduct = '';
        this.products = [];
        this.selectedProduct = {};
        this.productsLoading = false;
        this.catalog = [];
    }
    mounted() {
        this.getSuppliers();
        this.getCatalog();
    }
    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res) => {
            this.catalog = res.data;
        });
    }
    async getSuppliers() {
        await SuppliersModel
            .get()
            .then((res) => {
            this.suppliers = res;
        });
    }
    openCreationModal(supplier = { title: '', description: '', products: [] }) {
        this.$refs.form['resetValidation']();
        this.supplierForEdit = new SuppliersModel(JSON.parse(JSON.stringify(supplier)));
        this.creationModal = true;
    }
    saveSupplier() {
        EventBus.$emit('showLoading', true);
        this.supplierForEdit
            .save()
            .then((res) => {
            this.creationModal = false;
            EventBus.$emit('showLoader');
            setTimeout(() => {
                EventBus.$emit('showMessage', { ...res, type: 'success' });
                this.getSuppliers();
            }, 1000);
        });
    }
    async deleteSupplier(supplier) {
        const answer = await this.$confirm("Вы уверены что хотите удалить поставщика?", {
            buttonTrueText: "Подтвердить",
            buttonFalseText: "Отмена"
        });
        if (answer) {
            supplier
                .delete()
                .then((res) => {
                this.getSuppliers();
            });
        }
    }
    getCategoryById(id) {
        return this.catalog.filter(item => item.id == id)[0];
    }
    selectProduct(item) {
        setTimeout(() => {
            this.selectedProduct = '';
            this.supplierForEdit.products.push(item);
        });
        setTimeout(() => {
            this.products = [];
        }, 100);
    }
    async removeProduct(index) {
        const answer = await this.$confirm("Вы уверены что хотите удалить товар?", {
            buttonTrueText: "Подтвердить",
            buttonFalseText: "Отмена"
        });
        if (answer) {
            if (this.supplierForEdit.id) {
                axios.put(`${process.env.VUE_APP_BACKEND_API}/warehouse/suppliers/${this.supplierForEdit.id}/unselect_product`, this.supplierForEdit.products[index])
                    .then((res) => {
                    this.creationModal = true;
                    this.supplierForEdit.products.splice(index, 1);
                });
            }
            else {
                setTimeout(() => {
                    this.creationModal = true;
                    this.supplierForEdit.products.splice(index, 1);
                });
            }
        }
    }
    checkDuplicates(product, queryText, itemText) {
        if (!this.supplierForEdit.products) {
            return true;
        }
        return !this.supplierForEdit.products.filter(item => item.id == product.id).length;
    }
    ProductSearch(val) {
        if (!val || !val.length) {
            this.products = [];
            return;
        }
        // Items have already been requested
        if (this.productsLoading)
            return;
        this.productsLoading = true;
        // Lazily load input items
        ProductModel
            .params({
            filter: val,
            supplier: 0
        })
            .first()
            .then(res => {
            this.products = res.rows;
        })
            .catch(err => {
            console.log(err);
        })
            .finally(() => (this.productsLoading = false));
    }
};
__decorate([
    Watch('searchProduct'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], WarehouseSuppliersController.prototype, "ProductSearch", null);
WarehouseSuppliersController = __decorate([
    Component({
        computed: mapState(['loggedUser'])
    })
], WarehouseSuppliersController);
export default WarehouseSuppliersController;
//# sourceMappingURL=warehouse.suppliers.controller.js.map