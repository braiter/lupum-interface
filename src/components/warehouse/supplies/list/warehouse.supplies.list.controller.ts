import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';

import { Watch } from 'vue-property-decorator';
import {mapState} from "vuex";

const SuppliersModel = require('@/components/warehouse/suppliers/warehouse.suppliers.model').default;
const SuppliesModel = require('@/components/warehouse/supplies/warehouse.supplies.model').default;
const ProductModel = require('@/components/product/product.model').default;

const EventBus = require('@/plugins/eventBus').default;

@Component({
    computed: mapState(['loggedUser']),
})
export default class WarehouseSuppliesListController extends Core {
    supplies: Array<any> = [];
    
    suppliers: Array<any> = [];
    selectedSupplier: number | null = null;
    supplierProducts: Array<any> = [];
    
    openedSupply: any = null;

    catalog: Array<any> = [];
    
    dateFilter: any = null;
    showCalendar: boolean = false;

    mounted() {
        this.getSuppliers();
        this.getCatalog();
    }

    async getCatalog() {
        this.catalog = await this.$store.dispatch('getCatalog');
    }
    
    async getSuppliers() {
        await SuppliersModel
            .get()
            .then((res: any) => {
                this.suppliers = res;
            });
    }
    
    selectSupplier(id) {
        this.selectedSupplier = id;

        this.getSupplierProducts();
    }
    
    async getSupplierProducts() {
        await ProductModel
            .params({
                supplier_id: this.selectedSupplier
            })
            .first()
            .then((res: any) => {
                this.supplierProducts = res.rows;

                this.getSupplierSupplies();
            });
    }

    async getSupplierSupplies() {
        EventBus.$emit('showLoader', true);
        
        let query: any = {
            supplier_id: this.selectedSupplier
        };
    
        if (this.dateFilter) {
            query.dateFrom = this.dateFilter[0];
            query.dateTo = this.dateFilter[1];
        }
        
        await SuppliesModel
            .params(query)
            .get()
            .then((res: any) => {
                this.supplies = res;

                this.supplies.forEach(supply => {
                    supply.products = this.getProductsForSupply(supply.products);
                });

                EventBus.$emit('showLoader', false, 500);
            });
    }

    getProductsForSupply(products) {
        let supplierProducts = this.supplierProducts
            .sort((currentItem, nextItem) => {
                return currentItem.category_id - nextItem.category_id
            });

        let filteredProducts = supplierProducts.filter(supplierProduct => {
            return products.find(product => supplierProduct.id == product.product_id);
        });

        filteredProducts = filteredProducts.map(filteredProduct => {
            return products.find(product => filteredProduct.id == product.product_id);
        });

        return filteredProducts;
    }

    get purchaseTotalPrice() {
        return this.supplies[ this.openedSupply ].products.reduce(
            (accumulator, product) => accumulator + (product.purchase_count * product.purchase_price),
            0
        );
    }
    
    getProductTitle(id) {
        if (this.supplierProducts.length) {
            return this.supplierProducts.find(product => product.id == id).title_en;
        }
        
        return '';
    }

    getProductCategory(id) {
        if (this.supplierProducts.length && this.catalog) {
            let product = this.supplierProducts.find(product => product.id == id);

            return this.catalog.find(category => product.category_id == category.id);
        }

        return '';
    }

    @Watch("showCalendar")
    onShowCalendarOpened(value: any) {
        if (!value) {
            this.getSupplierSupplies();
        }
    }
}

