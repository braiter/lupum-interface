import { __decorate, __metadata } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import step1 from '../editSupplyStep/editSupply.step.1.vue';
import step2 from '../editSupplyStep/editSupply.step.2.vue';
import step3 from '../editSupplyStep/editSupply.step.3.vue';
import { Watch } from 'vue-property-decorator';
import { mapState } from "vuex";
const SuppliesModel = require('@/components/warehouse/supplies/warehouse.supplies.model').default;
const EventBus = require('@/plugins/eventBus').default;
let WarehouseSuppliesListController = class WarehouseSuppliesController extends Core {
    constructor() {
        super(...arguments);
        this.supplies = [];
        this.totalPages = 0;
        this.page = 1;
        this.supplyModal = false;
        this.supplyStatus = [
            'blue-grey darken-1',
            'lime lighten-3',
            'green lighten-2',
            'red lighten-2'
        ];
        this.months = ['Янв', 'Фев', 'Мрт', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Нояб', 'Дек'];
        this.createSupplyStep = 1;
        this.supplyForEdit = {};
        this.month = new Date();
    }
    mounted() {
        EventBus.$on('supplierStep', step => this.createSupplyStep = step);
        EventBus.$on('saveSupply', data => { this.saveSupply(data); });
        this.getSupplies();
    }
    async getSupplies() {
        let Dates = this.getDates(this.month);
        await SuppliesModel
            .params({
            from: this.formatDate(Dates.firstDay),
            until: this.formatDate(Dates.lastDay)
        })
            .first()
            .then((res) => {
            this.supplies = res;
            this.supplies['supplies']
                .forEach(item => {
                item.totalWholesalePrice = item.products
                    .map(product => (product.wholesale_price * product.product_count).toFixed(2))
                    .reduce((a, b) => parseFloat(a) + parseFloat(b), 0);
                item.totalRetailPrice = item.products
                    .map(product => (product.retail_price * product.product_count).toFixed(2))
                    .reduce((a, b) => parseFloat(a) + parseFloat(b), 0);
            });
        });
    }
    getDates(date) {
        return {
            firstDay: new Date(date.getFullYear(), date.getMonth(), 1),
            lastDay: new Date(date.getFullYear(), date.getMonth() + 1, 0)
        };
    }
    formatDate(date) {
        let d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }
    formatDateToRu(date) {
        let d = new Date(date), month = this.months[d.getMonth()], day = '' + d.getDate(), year = d.getFullYear();
        return [day, month, year].join(' ') + ` ${d.getHours()}:${d.getMinutes()}`;
    }
    editSupply(supply = {}) {
        this.supplyForEdit = supply;
        this.supplyModal = true;
        this.createSupplyStep = 1;
    }
    saveSupply(data) {
        let supply = new SuppliesModel({
            ...this.supplyForEdit,
            products: data.products,
            supplier_id: data.supplierId,
            status: data.status
        });
        supply
            .save()
            .then((res) => {
            EventBus.$emit('showMessage', { ...res, type: 'success' });
            this.supplyModal = false;
            this.getSupplies();
        });
    }
    modalChanged(val) {
        if (!val) {
            this.supplyForEdit = {};
        }
    }
};
__decorate([
    Watch('supplyModal'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], WarehouseSuppliesListController.prototype, "modalChanged", null);
WarehouseSuppliesListController = __decorate([
    Component({
        components: {
            'step-1': step1,
            'step-2': step2,
            'step-3': step3,
        },
        computed: mapState(['loggedUser']),
    })
], WarehouseSuppliesListController);
export default WarehouseSuppliesListController;
//# sourceMappingURL=warehouse.supplies.list.controller.js.map