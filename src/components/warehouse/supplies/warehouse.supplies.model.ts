import Model from "@/plugins/Model";

export default class WarehouseSuppliesModel extends Model {
    resource() {
        return "warehouse/supplies"
    }
}
