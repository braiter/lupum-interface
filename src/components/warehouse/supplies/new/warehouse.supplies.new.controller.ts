import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import { Prop, Watch } from 'vue-property-decorator';
const EventBus = require('@/plugins/eventBus').default;

const SuppliesModel = require('@/components/warehouse/supplies/warehouse.supplies.model').default;
const CatalogModel = require('@/components/catalog/catalog.model').default;

import selectProducts from './selectProducts/warehouse.supplies.selectProducts.vue';

@Component({
    components: {
        'select-products': selectProducts
    }
})
export default class EditSuppliesController extends Core {
    catalog: Array<any> = [];
    
    productsModal: boolean = false;
    products: Array<any> = [];
    selectedProducts: Array<any> = [];
    
    selectedSupplier: number = 0;
    
    filter: string = '';
    
    mounted() {
        this.getCatalog();
        
        EventBus.$on("selectProducts", (data) => {
            this.selectedProducts = data.products;
            this.selectedSupplier = data.supplier;
            
            this.selectedProducts = this.selectedProducts.map(item => {
                return {
                    ...item,
                    purchase_price: 0,
                    purchase_count: 0
                };
            });
    
            this.productsModal = false;
        });
    
        EventBus.$on("closeModal", () => {
            this.productsModal = false;
        });
    }
    
    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res: any) => {
                this.catalog = res.data;
            });
    }
    
    updatePrices(product) {
        if (!product.old_wholesale_price && !product.old_retail_price) {
            product.old_wholesale_price = product.wholesale_price;
            product.old_retail_price = product.retail_price;
        }
        
        product.wholesale_price = Math.floor(parseInt(product.purchase_price) + (product.purchase_price * this.getCategoryForProduct(product.category_id).category_wholesale_price_percantage / 100));
        product.retail_price = Math.floor(parseInt(product.purchase_price) + (product.purchase_price * this.getCategoryForProduct(product.category_id).category_retail_price_percantage / 100));
    }
    
    getCategoryForProduct(categoryId: number) {
        return this.catalog.find(category => category.id === categoryId);
    }
    
    get purchaseTotalPrice() {
        return this.selectedProducts.reduce(
            (accumulator, product) => accumulator + (product.purchase_count * product.purchase_price),
            0
        );
    }
    
    createNewSupply() {
        let supply = new SuppliesModel({
            products: this.selectedProducts,
            supplier_id: this.selectedSupplier
        });
        
        supply
            .save()
            .then(() => {
                this.$router.push(`/warehouse/supplies/list`);
            });
    }
    
    @Watch('supplyId')
    isSupplierId(val: any) {
        // if (this.supplyId) {
        //     this.selectedSupplier = this.suppliers.filter(item => item.id == this.supplierId)[0];
        //     this.status = JSON.parse(JSON.stringify(this.supplyStatus));
        //
        //     console.log(this.status);
        // }
    }
}
