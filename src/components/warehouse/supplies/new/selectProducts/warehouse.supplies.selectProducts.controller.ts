import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import { Prop, Watch } from 'vue-property-decorator';
import axios from "axios";
const EventBus = require('@/plugins/eventBus').default;

const SuppliersModel = require('@/components/warehouse/suppliers/warehouse.suppliers.model').default;
const ProductModel = require('@/components/product/product.model').default;
const CatalogModel = require('@/components/catalog/catalog.model').default;

@Component
export default class EditSuppliesController extends Core {
    @Prop() modal: any;
    
    suppliers: Array<any> = [];
    supplierId: any = this.$route.params.id;
    selectedSupplier: number | null = null;
    
    catalog: Array<any> = [];
    flatCatalog: Array<any> = [];
    selectedCategories: Array<any> = [];
    
    products: Array<any> = [];
    productsLoading: boolean = false;
    selectedProducts: Array<any> = [];
    
    filter: string = '';
    
    headers : Array<any> = [
        { text: 'Title RU', value: 'title_ru' },
        { text: 'Title EN', value: 'title_en' }
    ];
    
    mounted() {
        this.getSuppliers();
        this.getCatalog();
    }
    
    async getSuppliers() {
        await SuppliersModel
            .get()
            .then((res: any) => {
                this.suppliers = res;
            });
    }
    
    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res: any) => {
                this.flatCatalog = res.data;
                
                this.flatCatalog.forEach(item => {
                    if (!item.parent) {
                        this.catalog.push({
                            id: item.id,
                            title: item.title,
                            children: [],
                            locked: true
                        });
                        
                        this.prepareCatalog(
                            this.catalog[this.catalog.length-1].id,
                            this.catalog[this.catalog.length-1].children,
                            res.data
                        );
                    }
                });
            });
    }
    
    prepareCatalog(id, children, arr) {
        arr.forEach(item => {
            if (item.parent == id) {
                children.push({
                    id: item.id,
                    title: item.title,
                    parent: item.parent,
                    locked: true,
                    children: []
                });
                
                this.prepareCatalog(
                    item.id,
                    children[children.length - 1].children,
                    arr
                )
            }
        })
    }
    
    async getProducts() {
        this.productsLoading = true;
        
        await ProductModel
            .params({
                categories_list: this.selectedCategories.length? this.selectedCategories : [0],
                supplier_id: this.selectedSupplier
            })
            .first()
            .then((res: any) => {
                setTimeout(() => {
                    this.productsLoading = false;
                    
                    this.$vuetify.goTo(0);
                    
                    this.products = res.rows;
                }, 1000);
            })
    }
    
    selectSupplier(id) {
        this.selectedSupplier = id;
        this.products = [];
        this.selectedProducts = [];
        
        this.getSupplierCategories()
    }
    
    async getSupplierCategories() {
        await axios
            .get(`${process.env.VUE_APP_BACKEND_API}/warehouse/suppliers/${this.selectedSupplier}/categories`)
            .then((res) => {
                this.selectedCategories = res.data;
                
                this.catalog.forEach(category => {
                    category.locked = this.selectedCategories.indexOf(category.id) == -1;
                    
                    if (category.children.length) {
                        this.filterSupplierCategories(category.children);
                    }
                    
                    this.$forceUpdate();
                });
            });
    }
    
    filterSupplierCategories(catalog) {
        catalog.forEach(category => {
            category.locked = this.selectedCategories.indexOf(category.id) == -1;
            
            this.$forceUpdate();
            
            if (category.children.length) {
                this.filterSupplierCategories(category.children);
            }
        });
    }
    
    get supplierName() {
        if (this.selectedSupplier) {
            return this.suppliers.find(supplier => supplier.id === this.selectedSupplier).title;
        }
        
        return '';
    }
    
    selectProducts() {
        EventBus.$emit("selectProducts", {
            products: this.selectedProducts,
            supplier: this.selectedSupplier
        });
    }
    
    closeModal() {
        EventBus.$emit("closeModal");
    }
    
    @Watch('modal')
    modalChanged(val: any) {
        if (!val) {
            this.selectedSupplier = null;
            this.selectedProducts = [];
            this.selectedCategories = [];
            this.products = [];
        }
    }
}
