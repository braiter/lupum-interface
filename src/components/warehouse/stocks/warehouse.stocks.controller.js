import { __decorate, __metadata } from "tslib";
import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import { Watch } from 'vue-property-decorator';
import Catalog from '@/components/catalog/list/catalog.list.vue';
import { mapState } from "vuex";
const StocksModel = require('@/components/warehouse/stocks/warehouse.stocks.model').default;
const CatalogModel = require('@/components/catalog/catalog.model').default;
const EventBus = require('@/plugins/eventBus').default;
let WarehouseStocksController = class WarehouseStocksController extends Core {
    constructor() {
        super(...arguments);
        this.stocks = [];
        this.totalPages = 0;
        this.page = 1;
        this.catalog = [];
        this.filter = '';
        this.filterLoading = false;
        this.headers = [
            { text: 'Название', value: 'title' },
            { text: 'Наличие', value: 'count' },
        ];
    }
    mounted() {
        this.getProducts(1);
        this.getCatalog();
    }
    async getProducts(page = this.page) {
        EventBus.$emit('showLoader', true);
        let filter = this.filter;
        await StocksModel
            .params({
            page,
            filter,
            category: this.$route.params.id
        })
            .first()
            .then((res) => {
            this.filterLoading = false;
            this.$vuetify.goTo(0);
            this.stocks = res.rows;
            this.totalPages = res.totalPages;
            this.page = res.page;
            EventBus.$emit('showLoader', false, 500);
        });
    }
    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res) => {
            this.catalog = res.data;
        });
    }
    getCaregoryById(id) {
        let filterResults = this.catalog.filter(item => item.id == id);
        if (!filterResults.length) {
            return { id: 0, title: '' };
        }
        return filterResults[0];
    }
    debounceSearch() {
        // cancel pending call
        clearTimeout(this.debounceTimer);
        this.filterLoading = true;
        // delay new call 500ms
        this.debounceTimer = setTimeout(() => {
            this.getProducts(1);
        }, 500);
    }
    filterChanged(value) {
        this.debounceSearch();
    }
};
__decorate([
    Watch('filter'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], WarehouseStocksController.prototype, "filterChanged", null);
WarehouseStocksController = __decorate([
    Component({
        computed: mapState(['loggedUser']),
        components: {
            'catalog': Catalog
        },
    })
], WarehouseStocksController);
export default WarehouseStocksController;
//# sourceMappingURL=warehouse.stocks.controller.js.map