import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';

import { Watch } from 'vue-property-decorator';

import Catalog from '@/components/catalog/list/catalog.list.vue';
import {mapState} from "vuex";


const StocksModel = require('@/components/warehouse/stocks/warehouse.stocks.model').default;
const CatalogModel = require('@/components/catalog/catalog.model').default;

const EventBus = require('@/plugins/eventBus').default;

@Component({
    computed: mapState(['loggedUser']),
    components: {
        'catalog': Catalog
    },
})
export default class WarehouseStocksController extends Core {
    stocks: Array<any> = [];
    totalPages: number = 0;
    page: number = 1;

    catalog: Array<any> = [];
    selectedCategory = [];

    debounceTimer: any;
    filter: string = '';
    filterLoading: boolean = false;

    headers : Array<any> = [
        { text: 'Title RU', value: 'title_ru' },
        { text: 'Title EN', value: 'title_en' },
        { text: 'Stock', value: 'count' }
    ];

    mounted() {
        this.getCatalog();
    }

    async getProducts(page: number = this.page) {
        EventBus.$emit('showLoader', true);

        await StocksModel
            .params({
                page,
                title: this.filter,
                category: this.selectedCategory[0]
            })
            .first()
            .then((res: any) => {
                this.filterLoading = false;

                this.$vuetify.goTo(0);

                this.stocks = res.rows;
                this.totalPages = res.totalPages;
                this.page = page;

                EventBus.$emit('showLoader', false, 500);
            })
    }

    async getCatalog() {
        await CatalogModel
            .getCatalog()
            .then((res: any) => {
                res.data.forEach(item => {
                    if (!item.parent) {
                        this.catalog.push({
                            id: item.id,
                            title: item.title,
                            children: []
                        });
            
                        this.prepareCatalog(
                            this.catalog[this.catalog.length-1].id,
                            this.catalog[this.catalog.length-1].children,
                            res.data
                        );
                    }
                });
            });
    }
    
    prepareCatalog(id, children, arr) {
        arr.forEach(item => {
            if (item.parent == id) {
                children.push({
                    id: item.id,
                    title: item.title,
                    parent: item.parent,
                    children: []
                });
            
                this.prepareCatalog(
                    item.id,
                    children[children.length - 1].children,
                    arr
                )
            }
        })
    }

    debounceSearch() {
        // cancel pending call
        clearTimeout(this.debounceTimer);

        // delay new call 500ms
        this.debounceTimer = setTimeout(() => {
            this.filterLoading = true;
            this.getProducts(1);
        }, 500)
    }

    @Watch('filter')
    filterChanged(value: any) {
        this.debounceSearch();
    }
}

