import Model from "@/plugins/Model";

export default class WarehouseStocksModel extends Model {
    resource() {
        return "warehouse/stocks"
    }
}
