import Component from 'vue-class-component';
import Core from '@/components/core/core.controller';
import {mapState} from "vuex";

@Component({
    computed: mapState(['loggedUser']),
})
export default class WarehouseSuppliesController extends Core {
    activeTab: string | null = null;
    
    mounted() {
        if (this.$router['history'].current.name.indexOf('warehouse.supplies') !== -1) {
            this.activeTab = '/warehouse/supplies/list';
        }
    }
}

