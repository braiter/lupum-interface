import Vue from "vue";

declare module '*.vue' {
	import Vue from 'vue';
	export default Vue;
}

declare module 'vue/types/options' {
	interface ComponentOptions<V extends Vue> {
		// this is required because current typings of vee-validate have the old $validates in them, which doesn't work anymore
		$_veeValidate?: any;
	}
}

declare module '*.html' {
	import Vue, { ComponentOptions } from 'vue';

	interface WithRender {
		<V extends Vue>(options: ComponentOptions<V>): ComponentOptions<V>
		<V extends typeof Vue>(component: V): V
	}

	const withRender: WithRender
	export = withRender
}
