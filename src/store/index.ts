import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const ConfigModel = require('@/components/config/config.model').default;
const CatalogModel = require('@/components/catalog/catalog.model').default;

export default new Vuex.Store({
    state: {
        isUserLogged: false,
        token: '',
        loggedUser: false,
        appConfig: null,
        catalog: []
    },
    mutations: {
        setUser(context, user: any) {
            context.loggedUser = user;
        },
        setConfig(context, config: any) {
            context.appConfig = config;
        }
    },
    actions: {
        setUser(context, user: any) {
            return new Promise((resolve, reject) => {
                context.commit('setUser', user);
                resolve();
            });
        },
        setConfig(context, config: any) {
            return new Promise((resolve, reject) => {
                context.commit('setConfig', config);
                resolve();
            });
        },
        getConfig(state: any) {
            return new Promise(async (resolve: any) => {
                if (state.appConfig) {
                    resolve(state.appConfig);
                } else {
                    let config = await ConfigModel.get();

                    state.appConfig = config;
                    resolve(config);
                }
            });
        },
        getCatalog(state: any) {
            return new Promise(async (resolve: any) => {
                if (state.state.catalog.length) {
                    resolve(state.state.catalog);
                } else {
                    let catalog = await CatalogModel
                        .getCatalog();

                    state.state.catalog = catalog.data;
                    resolve(catalog.data);
                }
            });
        }
    },
    modules: {
    },
    getters: {
        getLoggedUser(state: any) {
            return new Promise((resolve: any) => {
                setInterval(() => {
                    resolve(state.loggedUser);
                }, 1000)
            });
        }
    }
})
