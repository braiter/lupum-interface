import Vue from 'vue'
import App from './components/app/app.vue'
import './typings/registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import axios from "axios";
import VuetifyConfirm    from 'vuetify-confirm';
import VueFusionCharts from 'vue-fusioncharts';
import FusionCharts from 'fusioncharts';
import Charts from 'fusioncharts/fusioncharts.timeseries'
import VueI18n from 'vue-i18n'
import "flag-icons";
import wysiwyg from "vue-wysiwyg";
import "vue-wysiwyg/dist/vueWysiwyg.css";

import en from './locales/en.json';
import ua from './locales/uk.json';
import ru from './locales/ru.json';
import gr from './locales/el_GR.json';

const messages = {
    en,
    ua,
    ru,
    gr
};

Vue.use(VueI18n);
Vue.use(wysiwyg, {});

const i18n = new VueI18n({
    locale: 'en', // set locale
    fallbackLocale: 'en', // set fallback locale
    messages, // our translations
});

// register VueFusionCharts component
Vue.use(VueFusionCharts, FusionCharts, Charts)

let ERROR_SHOWED = false;

Vue.config.productionTip = false;

Vue.use(VuetifyConfirm, { vuetify });

axios.interceptors.request.use(
    (config: any) => {
        let token = localStorage.token;

        if (!!token &&
            config.url.indexOf('novaposhta') == -1 &&
            config.url.indexOf('sat') == -1
        ) {
            config.headers.Authorization = "Bearer " + token;
        }

        config.headers.Accept = "application/json";

        return config;
    },
    error => Promise.reject(error));

axios.interceptors.response.use(
  config => config,
  error => {
    if (error.response.status === 401 && !Vue.prototype.criticalError) {
      Vue.prototype.criticalError = true;
      router.push('/logout');
    }
    else if (error.response.status == 404) {
      Vue.prototype.criticalError = true;
    }
    else if (error.response.status == 500) {

    }

        return Promise.reject(error);
    });


new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
