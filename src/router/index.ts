import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: '/login',
        name: 'login',
        component: () => import('@/components/user/login/user.login.vue')
    },
    {
        path: '/logout',
        name: 'logout',
        component: () => import('@/components/user/logout/user.logout.vue')
    },
    {
        path: '',
        name: 'innerPage',
        component: () => import('@/components/innerPage/innerPage.vue'),
        redirect: 'dashboard',
        children: [
            {
                path: '/dashboard',
                name: 'dashboard',
                component: () => import('@/components/dashboard/dashboard.vue'),
            },
            {
                path: '/user/profile/:id',
                name: 'myUserProfile',
                component: () => import('@/components/user/profile/user.profile.vue'),
            },
            {
                path: '/user/profile',
                name: 'userProfile',
                component: () => import('@/components/user/profile/user.profile.vue'),
            },
            {
                path: '/users',
                name: 'userList',
                component: () => import('@/components/user/list/user.list.vue'),
            },
            {
                path: '/catalog',
                name: 'catalogViewer',
                component: () => import('@/components/catalog/list/catalog.list.vue'),
            },
            {
                path: '/catalog/editor',
                name: 'catalogEditor',
                component: () => import('@/components/catalog/editor/catalog.editor.vue'),
            },
            {
                path: '/catalog/:id/products',
                name: 'productsList',
                component: () => import('@/components/product/list/product.list.vue'),
            },
            {
                path: '/massive-creation',
                name: 'productsList',
                component: () => import('@/components/massiveCreation/massiveCreation.vue'),
            },
            {
                path: '/warehouse',
                name: 'warehouse',
                component: () => import('@/components/warehouse/warehouse.vue'),
                children: [
                    {
                        path: 'stocks',
                        name: 'warehouse.stock',
                        component: () => import('@/components/warehouse/stocks/warehouse.stocks.vue'),
                    },
                    {
                        path: 'supplies',
                        name: 'warehouse.supplies',
                        component: () => import('@/components/warehouse/supplies/warehouse.supplies.vue'),
                        children: [
                            {
                                path: 'new',
                                name: 'warehouse.supplies.new',
                                component: () => import('@/components/warehouse/supplies/new/warehouse.supplies.new.vue'),
                            },
                            {
                                path: 'list',
                                name: 'warehouse.supplies.list',
                                component: () => import('@/components/warehouse/supplies/list/warehouse.supplies.list.vue'),
                            },
                        ]
                    },
                    {
                        path: 'suppliers',
                        name: 'warehouse.suppliers',
                        component: () => import('@/components/warehouse/suppliers/warehouse.suppliers.vue'),
                    }
                ]
            },
			{
				path: '/orders',
				name: 'orderList',
				component: () => import('@/components/orders/orders.vue'),
			},
            {
                path: '/checkout',
                name: 'checkout',
                component: () => import('@/components/checkout/checkout.vue')
            },
            {
                path: '/config',
                name: 'config',
                component: () => import('@/components/config/config.vue')
            }
        ]
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
