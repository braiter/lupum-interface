export default interface ModelInterface {
    get?: Promise<any>;
    config?: Promise<any>;
    baseURL: () => any;
}
