import Vue from 'vue'
import Vuetify from 'vuetify/lib';
// import 'vuetify/src/stylus/app.styl'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/css/v4-shims.css'// Ensure you are using css-loader
// import Ripple from 'ripple-lib'
import { Ripple } from 'vuetify/lib/directives'

import 'element-ui/lib/theme-chalk/index.css';

Vue.use(Vuetify);

export default new Vuetify({
	icons: {
		iconfont: 'mdi',  // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
	},
	theme: {
		dark: false,
		themes: {
			light: {
				primary: "#4682b4",
				secondary: "#b0bec5",
				accent: "#8c9eff",
				error: "#b71c1c",
				success: "#388E3C"
			},
		},
	},
})
