import { Model as BaseModel } from "vue-api-query";
import axios from "axios";
// inject global axios instance as http client to Model
BaseModel.$http = axios;
let HEADERS = null;
export default class Model extends BaseModel {
    // define a base url for a REST API
    baseURL() {
        return process.env.VUE_APP_BACKEND_API;
    }
    // implement a default request method
    request(config) {
        config.withCredentials = true;
        let request = this.$http.request(config);
        request.then((res) => {
            HEADERS = res.headers;
        });
        return request;
    }
    getHeaders() {
        return HEADERS;
    }
}
//# sourceMappingURL=Model.js.map